export function coloredSpan(color: string, content: string): string {
  return `<span style="background-color: ${color}; color: white">${content}</span>`
}

export function emojizedText(emoji: string, content: string): string {
  return emoji ? `${content} ${emoji}` : content
}
