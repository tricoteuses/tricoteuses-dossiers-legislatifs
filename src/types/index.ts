import { CodeActe, CodeTypeOrgane } from "@tricoteuses/assemblee"

import { AssembleeStep, Dossier, StepActe, StepOnlyTypeActe, StepTypeActe } from "./assemblee"
import { LecAssSimple, LectureSimple, Loi, SenatStep, SenatStepKind, TexteStep } from "./senat"
import { StepCodeTypeOrgane, StepOnlyCodeTypeOrgane } from "./shared"

export {
  AssembleeStep,
  CodeTypeOrgane,
  Dossier,
  LecAssSimple,
  LectureSimple,
  Loi,
  SenatStep,
  SenatStepKind,
  StepActe,
  StepCodeTypeOrgane,
  StepOnlyCodeTypeOrgane,
  StepOnlyTypeActe,
  StepTypeActe,
  TexteStep,
}

/// First level blocks of chronology
export interface DateBlock {
  dateLocalIsoString?: string
  keys: string[]
  organeBlockByKey: { [key: string]: OrganeBlock }
}

export interface DossierAndLoi {
  dossier?: Dossier
  loi?: Loi
}

/// Second level blocks of chronology
export interface OrganeBlock {
  id?: string // Used only by Assemblée. TODO: Remove or keep?
  keys: string[]
  organeCodeType: StepCodeTypeOrgane
  organeName: string
  originOrganeCodeType?: StepCodeTypeOrgane
  originOrganeName?: string
  subOrganeBlockByKey: { [key: string]: SubOrganeBlock }
  title?: string
}

/// Third level blocks of chronology (sub-organes equals "commissions", etc)
export interface SubOrganeBlock {
  assembleeSteps?: AssembleeStep[]
  codeActe?: CodeActe // Used only by Assemblée. TODO: Remove or keep?
  id?: string // Used only by Assemblée. TODO: Remove or keep?
  senatSteps?: SenatStep[]
  title: string
}
