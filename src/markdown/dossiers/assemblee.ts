import {
  CodeTypeOrgane,
  Division,
  Document,
  DocumentUrlFormat,
  shortNameFromOrgane,
  Status,
  statusFromCodierLibelle,
  StatusOnly,
  TypeActeLegislatif,
  TypeDocument,
  uncapitalizeFirstLetter,
  urlFromDocument,
  urlFromScrutin,
} from "@tricoteuses/assemblee"

import config from "../../config"
import {
  frenchDateWithoutDayOfWeekFromDateOrString,
  frenchTimeFromDateOrString,
} from "../../dates"
import { emojizedText } from "../shared"
import {
  AssembleeStep,
  StepOnlyTypeActe,
  StepCodeTypeOrgane,
  StepOnlyCodeTypeOrgane,
} from "../../types"

export function colorFromCodierLibelle(libelle: string): string {
  const status = statusFromCodierLibelle(libelle)
  return colorFromStatus(status)
}

export function colorFromOrganeCodeType(code: StepCodeTypeOrgane): string {
  switch (code) {
    case CodeTypeOrgane.Assemblee:
      // Color taken from http://www.assemblee-nationale.fr/
      return "#023e6a"
    case StepOnlyCodeTypeOrgane.Ce:
      // Conseil d'État
      // Color taken from http://www.conseil-etat.fr/
      return "#b1b3b4"
    case CodeTypeOrgane.Cmp:
      // Commission mixte paritaire
      return "#975a16" // yellow-800
    case CodeTypeOrgane.Constitu:
      // Conseil constitutionnel
      // Color taken from https://www.conseil-constitutionnel.fr/
      return "#b29762"
    case CodeTypeOrgane.Gouvernement:
      // Color taken from https://www.gouvernement.fr/
      return "#202328"
    case StepOnlyCodeTypeOrgane.Internet:
      return "#38b2ac" // teal-500
    case CodeTypeOrgane.Presrep:
      // Présidence de la République
      // Color taken from https://www.elysee.fr/
      return "#000"
    case CodeTypeOrgane.Senat:
      // Color taken from http://www.senat.fr/
      return "#c81f48"
    default:
      return "#975a16" // yellow-800
  }
}

export function colorFromStatus(status: Status): string {
  switch (status) {
    case StatusOnly.Failure:
      return "#f56565" // red-500
    case StatusOnly.InProgress:
      return "#718096" // gray-600
    case StatusOnly.Success:
      return "#48bb78" // green-500
    default:
      return colorFromOrganeCodeType(status as StepCodeTypeOrgane)
  }
}

export function emojiFromCodierLibelle(libelle: string): string {
  const status = statusFromCodierLibelle(libelle)
  return emojiFromStatus(status)
}

export function emojiFromOrganeCodeType(code: StepCodeTypeOrgane): string {
  switch (code) {
    default:
      return ""
  }
}

export function emojiFromStatus(status: Status): string {
  switch (status) {
    case StatusOnly.Failure:
      return ":x:"
    case StatusOnly.InProgress:
      return ":arrow_right:"
    case StatusOnly.Success:
      return ":white_check_mark:"
    default:
      return emojiFromOrganeCodeType(status as StepCodeTypeOrgane)
  }
}

function markdownLinesFromActeDocument(
  lines: string[],
  document: Document | Division,
  libelle?: string,
  depth: number = 0,
) {
  const fragments = []
  if (libelle !== undefined) {
    fragments.push(libelle)
    fragments.push(":")
  }
  switch (document.xsiType) {
    case TypeDocument.AccordInternationalType: {
      fragments.push(
        `TODO _Document de type inconnu_ : ${document.xsiType}`,
        `\`\`\`json\n${JSON.stringify(document, null, 2)}\n\`\`\``,
      )
      break
    }
    case TypeDocument.AvisConseilEtatType: {
      let documentTitle = document.titres.titrePrincipal
      const pdfUrl = urlFromDocument(document, DocumentUrlFormat.Pdf)
      if (pdfUrl !== null) {
        documentTitle = `[${document.titres.titrePrincipal} (PDF)](${pdfUrl})`
      }
      fragments.push(documentTitle)
      break
    }
    case TypeDocument.DocumentEtudeImpactType: {
      let documentTitle = document.titres.titrePrincipal
      const htmlUrl = urlFromDocument(document, DocumentUrlFormat.Html)
      if (htmlUrl !== null) {
        documentTitle = `[${document.titres.titrePrincipal}](${htmlUrl})`
      }
      fragments.push(documentTitle)
      const pdfUrl = urlFromDocument(document, DocumentUrlFormat.Pdf)
      if (pdfUrl !== null) {
        fragments.push(`([version PDF)](${pdfUrl})`)
      }
      break
    }
    case TypeDocument.RapportParlementaireType: {
      const titleFragments = [document.denominationStructurelle]
      if (document.notice.numNotice !== undefined) {
        titleFragments.push("n°", document.notice.numNotice)
      }
      if (document.notice.formule !== undefined) {
        titleFragments.push(document.notice.formule)
      }
      let documentTitle = titleFragments.join(" ")
      const htmlUrl = urlFromDocument(document, DocumentUrlFormat.Html)
      if (htmlUrl !== null) {
        documentTitle = `[${document.titres.titrePrincipal}](${htmlUrl})`
      }
      fragments.push(documentTitle)
      const pdfUrl = urlFromDocument(document, DocumentUrlFormat.Pdf)
      if (pdfUrl !== null) {
        fragments.push(`([version PDF)](${pdfUrl})`)
      }
      break
    }
    case TypeDocument.TexteLoiType: {
      const titleFragments = [document.denominationStructurelle]
      if (document.notice.numNotice !== undefined) {
        titleFragments.push("n°", document.notice.numNotice)
      }
      if (document.notice.formule !== undefined) {
        titleFragments.push(document.notice.formule)
      }
      let documentTitle = titleFragments.join(" ")
      fragments.push(
        `[${documentTitle}](${new URL(
          `documents/assemblee/${document.uid}`,
          config.uiUrl,
        ).toString()})`,
      )
      const pdfUrl = urlFromDocument(document, DocumentUrlFormat.Pdf)
      if (pdfUrl !== null) {
        fragments.push(`([version PDF)](${pdfUrl})`)
      }
      break
    }
  }
  if (depth === 0) {
    lines.push(fragments.join(" "))
  } else {
    lines.push("  ".repeat(depth) + "* " + fragments.join(" "))
  }
  lines.push("")

  const divisions = document.divisions
  if (divisions !== undefined) {
    for (const division of divisions) {
      markdownLinesFromActeDocument(lines, division, undefined, depth + 1)
    }
  }
}

export function markdownLinesFromAssembleeStep(
  lines: string[],
  step: AssembleeStep,
): void {
  switch (step.typeActe) {
    case TypeActeLegislatif.ConclusionEtapeCcType: {
      const leafActe = step.leafActe
      const statutConclusion = leafActe.statutConclusion!
      const fragments = [
        `[Décision n° ${leafActe.anneeDecision}-${leafActe.numDecision}](${leafActe.urlConclusion}) :`,
        emojizedText(
          emojiFromCodierLibelle(statutConclusion.libelle),
          statutConclusion.libelle,
        ),
      ]
      lines.push(fragments.join(" "))
      lines.push("")
      break
    }
    case TypeActeLegislatif.DecisionType: {
      const leafActe = step.leafActe
      const libelleActe = leafActe.libelleActe
      const fragments = [
        (libelleActe.libelleCourt || libelleActe.nomCanonique).replace(/\.$/, ""),
      ]
      if (leafActe.time !== undefined) {
        fragments.push(`à ${leafActe.time}`)
      }
      fragments.push(":")
      const statutConclusion = leafActe.statutConclusion
      if (statutConclusion !== undefined) {
        fragments.push(
          emojizedText(
            emojiFromCodierLibelle(statutConclusion.libelle),
            statutConclusion.libelle,
          ),
        )
      }
      lines.push(fragments.join(" "))
      lines.push("")
      if (leafActe.votes !== undefined) {
        for (const vote of leafActe.votes) {
          lines.push(`* [Vote sur ${vote.titre}](${urlFromScrutin(vote)})`)
        }
      }
      lines.push("")
      break
    }
    case TypeActeLegislatif.DecisionMotionCensureType: {
      const leafActe = step.leafActe
      const libelleActe = leafActe.libelleActe
      const fragments = [
        (libelleActe.libelleCourt || libelleActe.nomCanonique).replace(/\.$/, ""),
      ]
      if (leafActe.time !== undefined) {
        fragments.push(`à ${leafActe.time}`)
      }
      fragments.push(":")
      const decision = leafActe.decision
      if (decision !== undefined) {
        fragments.push(
          emojizedText(emojiFromCodierLibelle(decision.libelle), decision.libelle),
        )
      }
      lines.push(fragments.join(" "))
      lines.push("")
      if (leafActe.votes !== undefined) {
        for (const vote of leafActe.votes) {
          lines.push(`* [Vote sur ${vote.titre}](${urlFromScrutin(vote)})`)
        }
      }
      lines.push("")
      break
    }
    case TypeActeLegislatif.DepotAvisConseilEtatType: {
      const leafActe = step.leafActe
      const texteAssocie = leafActe.texteAssocie
      if (texteAssocie !== undefined) {
        markdownLinesFromActeDocument(lines, texteAssocie)
      } else {
        const libelleActe = leafActe.libelleActe
        const libelle = (libelleActe.libelleCourt || libelleActe.nomCanonique).replace(
          /\.$/,
          "",
        )
        lines.push(libelle)
        lines.push("")
      }
      break
    }
    case TypeActeLegislatif.DepotInitiativeType: {
      const leafActe = step.leafActe
      const libelleActe = leafActe.libelleActe
      const libelle = (libelleActe.libelleCourt || libelleActe.nomCanonique).replace(
        /\.$/,
        "",
      )
      const texteAssocie = leafActe.texteAssocie
      if (texteAssocie !== undefined) {
        markdownLinesFromActeDocument(lines, texteAssocie, libelle)
      } else {
        lines.push(libelle)
        lines.push("")
      }
      break
    }
    case TypeActeLegislatif.DepotInitiativeNavetteType: {
      const leafActe = step.leafActe
      const libelleActe = leafActe.libelleActe
      const libelle = (libelleActe.libelleCourt || libelleActe.nomCanonique).replace(
        /\.$/,
        "",
      )
      const texteAssocie = leafActe.texteAssocie
      if (texteAssocie !== undefined) {
        markdownLinesFromActeDocument(lines, texteAssocie, libelle)
      } else {
        lines.push(libelle)
        lines.push("")
      }
      break
    }
    case TypeActeLegislatif.DepotLettreRectificativeType: {
      const leafActe = step.leafActe
      const libelleActe = leafActe.libelleActe
      const libelle = (libelleActe.libelleCourt || libelleActe.nomCanonique).replace(
        /\.$/,
        "",
      )
      const texteAssocie = leafActe.texteAssocie
      if (texteAssocie !== undefined) {
        markdownLinesFromActeDocument(lines, texteAssocie, libelle)
      } else {
        lines.push(libelle)
        lines.push("")
      }
      break
    }
    case TypeActeLegislatif.DepotRapportType: {
      const leafActe = step.leafActe
      const texteAdopte = leafActe.texteAdopte
      if (texteAdopte !== undefined) {
        markdownLinesFromActeDocument(
          lines,
          texteAdopte,
          "Texte adopté par la commission",
        )
      }
      const libelleActe = leafActe.libelleActe
      const libelle = (libelleActe.libelleCourt || libelleActe.nomCanonique).replace(
        /\.$/,
        "",
      )
      const texteAssocie = leafActe.texteAssocie
      if (texteAssocie !== undefined) {
        markdownLinesFromActeDocument(lines, texteAssocie, libelle)
      } else {
        lines.push(libelle)
        lines.push("")
      }
      break
    }
    case TypeActeLegislatif.DiscussionCommissionType: {
      const leafActe = step.leafActe
      const libelleActe = leafActe.libelleActe
      const libelle = (libelleActe.libelleCourt || libelleActe.nomCanonique).replace(
        /\.$/,
        "",
      )
      const reunion = leafActe.reunion
      {
        const fragments = [libelle]
        if (reunion !== undefined && reunion.timestampDebut !== undefined) {
          if (reunion.timestampFin === undefined) {
            fragments.push(" à ", frenchTimeFromDateOrString(reunion.timestampDebut))
          } else {
            fragments.push(
              " de ",
              frenchTimeFromDateOrString(reunion.timestampDebut),
              " à ",
              frenchTimeFromDateOrString(reunion.timestampFin),
            )
          }
        } else if (leafActe.time !== undefined) {
          fragments.push(" à ", leafActe.time)
        }
        if (reunion !== undefined) {
          if (reunion.lieu !== undefined) {
            const libelleLieu = reunion.lieu.libelleLong || reunion.lieu.libelleCourt
            if (libelleLieu !== undefined) {
              fragments.push(", ", libelleLieu)
            }
          }
          if (reunion.ouverturePresse) {
            fragments.push(", _ouvert à la presse_")
          } else {
            fragments.push(", _fermé à la presse_")
          }
          const odj = leafActe.odj
          if (odj === undefined) {
            const cycleDeVie = reunion.cycleDeVie
            if (cycleDeVie !== undefined) {
              fragments.push(`, statut : ${cycleDeVie.etat}`)
            }
          } else {
            if (odj.comiteSecret) {
              fragments.push(", _point en comité secret_")
            }
            const cycleDeVie = odj.cycleDeVie
            if (cycleDeVie !== undefined) {
              fragments.push(`, statut : ${cycleDeVie.etat}`)
            }
          }
        }
        lines.push(fragments.join(""))
        lines.push("")
      }
      break
    }
    case TypeActeLegislatif.DiscussionSeancePubliqueType: {
      const leafActe = step.leafActe
      const libelleActe = leafActe.libelleActe
      const libelle = (libelleActe.libelleCourt || libelleActe.nomCanonique).replace(
        /\.$/,
        "",
      )
      const odj = leafActe.odj
      const reunion = leafActe.reunion
      {
        const fragments = [libelle]
        if (reunion !== undefined && reunion.timestampDebut !== undefined) {
          if (reunion.timestampFin === undefined) {
            fragments.push(" à ", frenchTimeFromDateOrString(reunion.timestampDebut))
          } else {
            fragments.push(
              " de ",
              frenchTimeFromDateOrString(reunion.timestampDebut),
              " à ",
              frenchTimeFromDateOrString(reunion.timestampFin),
            )
          }
        } else if (leafActe.time !== undefined) {
          fragments.push(" à ", leafActe.time)
        }
        if (reunion !== undefined) {
          const identifiants = reunion.identifiants
          if (identifiants !== undefined) {
            fragments.push(`, ${identifiants.quantieme} séance`)
            if (identifiants.numSeanceJo !== undefined) {
              fragments.push(` (n° ${identifiants.numSeanceJo})`)
            }
          }
          if (reunion.ouverturePresse) {
            fragments.push(", _ouvert à la presse_")
          } else {
            fragments.push(", _fermé à la presse_")
          }
          if (odj === undefined) {
            const cycleDeVie = reunion.cycleDeVie
            if (cycleDeVie !== undefined) {
              fragments.push(`, statut : ${cycleDeVie.etat}`)
            }
          } else {
            if (odj.comiteSecret) {
              fragments.push(", _point en comité secret_")
            }
            const cycleDeVie = odj.cycleDeVie
            if (cycleDeVie !== undefined) {
              fragments.push(`, statut : ${cycleDeVie.etat}`)
            }
          }
        }
        lines.push(fragments.join(""))
        lines.push("")

        if (odj !== undefined) {
          const objet = odj.objet || odj.typePointOdj
          lines.push("> _objet_ : " + objet)
          lines.push("")
        }
      }
      break
    }
    case TypeActeLegislatif.EtudeImpactType: {
      const leafActe = step.leafActe
      const texteAssocie = leafActe.texteAssocie
      if (texteAssocie !== undefined) {
        markdownLinesFromActeDocument(lines, texteAssocie)
      } else {
        const libelleActe = leafActe.libelleActe
        const libelle = (libelleActe.libelleCourt || libelleActe.nomCanonique).replace(
          /\.$/,
          "",
        )
        lines.push(libelle)
        lines.push("")
      }
      break
    }
    case StepOnlyTypeActe.EtudeImpactContributionInternauteFermeture: {
      lines.push("Fin de la consultation en ligne des citoyens")
      lines.push("")
      break
    }
    case StepOnlyTypeActe.EtudeImpactContributionInternauteOuverture: {
      lines.push("Début de la consultation en ligne des citoyens")
      lines.push("")
      break
    }
    case TypeActeLegislatif.NominRapporteursType: {
      const leafActe = step.leafActe
      const libelleActe = leafActe.libelleActe
      const libelle = (libelleActe.libelleCourt || libelleActe.nomCanonique).replace(
        /\.$/,
        "",
      )
      lines.push(libelle)
      lines.push("")

      if (leafActe.rapporteurs !== undefined) {
        for (const rapporteur of leafActe.rapporteurs) {
          const acteur = rapporteur.acteur
          if (acteur === undefined) {
            lines.push(
              `TODO _Rapporteur inconnu _ :`,
              "",
              `\`\`\`json\n${JSON.stringify(rapporteur, null, 2)}\n\`\`\``,
              "",
            )
          } else {
            const ident = acteur.etatCivil.ident
            const photo = acteur.photo
            if (photo !== undefined) {
              lines.push(
                "<table>",
                "  <tbody>",
                "    <tr>",
                "      <td>",
                `![${ident.prenom} ${ident.nom}](${new URL(
                  photo.chemin,
                  config.assembleeApiUrl,
                ).toString()})`,
                "      </td>",
                "      <td>",
              )
            }

            const fragments = [
              `[${ident.prenom} ${ident.nom}](${new URL(
                `personnes/assemblee/${acteur.uid}`,
                config.uiUrl,
              ).toString()})`,
              `(${rapporteur.typeRapporteur})`,
            ]
            lines.push(fragments.join(" "))
            // TODO: Add groupePolitique.

            if (photo !== undefined) {
              lines.push("      </td>", "    </tr>", "  </tbody>", "</table>")
            }
            lines.push("")
          }
        }
      }
      break
    }
    case TypeActeLegislatif.ProcedureAccelereType: {
      const leafActe = step.leafActe
      const libelleActe = leafActe.libelleActe
      const libelle = (libelleActe.libelleCourt || libelleActe.nomCanonique).replace(
        /\.$/,
        "",
      )
      lines.push(libelle)
      lines.push("")
      break
    }
    case TypeActeLegislatif.PromulgationType: {
      const leafActe = step.leafActe
      lines.push(`Promulgation de la loi n° ${leafActe.codeLoi}`)
      lines.push("")
      break
    }
    case StepOnlyTypeActe.PromulgationInfoJo: {
      const leafActe = step.leafActe
      const infoJo = leafActe.infoJo!
      lines.push(
        `Parution au [Journal officiel n° ${infoJo.numJo}](${
          infoJo.urlLegifrance
        }) de la loi n° ${
          leafActe.codeLoi
        } du ${frenchDateWithoutDayOfWeekFromDateOrString(leafActe.dateActe!)} ${
          leafActe.titreLoi
        }`,
      )
      lines.push("")
      break
    }
    case StepOnlyTypeActe.PromulgationInfoJoRect: {
      const leafActe = step.leafActe
      const infoJo = leafActe.infoJo!
      lines.push(
        `Parution au [Journal officiel n° ${infoJo.numJo}](${
          infoJo.urlLegifrance
        }) d'un rectificatif à la loi n° ${
          leafActe.codeLoi
        } du ${frenchDateWithoutDayOfWeekFromDateOrString(leafActe.dateActe!)} ${
          leafActe.titreLoi
        }`,
      )
      lines.push("")
      break
    }
    case TypeActeLegislatif.RenvoiCmpType: {
      const leafActe = step.leafActe
      lines.push(`Convocation ${leafActe.organe!.libelleEdition}`)
      lines.push("")
      break
    }
    case TypeActeLegislatif.SaisieComAvisType: {
      const leafActe = step.leafActe
      const libelleActe = leafActe.libelleActe
      const libelle = (libelleActe.libelleCourt || libelleActe.nomCanonique).replace(
        /\.$/,
        "",
      )
      const fragments = [libelle]
      if (leafActe.codeActe.includes("-COM-")) {
        fragments.push(
          ":",
          `${leafActe.organe!.libelle} (${shortNameFromOrgane(leafActe.organe)})`,
        )
      }
      lines.push(fragments.join(" "))
      lines.push("")
      break
    }
    case TypeActeLegislatif.SaisieComFondType: {
      const leafActe = step.leafActe
      const libelleActe = leafActe.libelleActe
      const libelle = (libelleActe.libelleCourt || libelleActe.nomCanonique).replace(
        /\.$/,
        "",
      )
      const fragments = [libelle]
      if (leafActe.codeActe.includes("-COM-")) {
        fragments.push(
          ":",
          `${leafActe.organe!.libelle} (${shortNameFromOrgane(leafActe.organe)})`,
        )
      }
      lines.push(fragments.join(" "))
      lines.push("")
      break
    }
    case TypeActeLegislatif.SaisineConseilConstitType: {
      const leafActe = step.leafActe
      const libelleActe = leafActe.libelleActe
      const libelle = (libelleActe.libelleCourt || libelleActe.nomCanonique).replace(
        /\.$/,
        "",
      )
      const fragments = [libelle]
      if (leafActe.casSaisine !== undefined) {
        fragments.push(` par ${uncapitalizeFirstLetter(leafActe.casSaisine.libelle)}`)
      }
      if (leafActe.motif !== undefined) {
        fragments.push(`, ${uncapitalizeFirstLetter(leafActe.motif)}`)
      }
      lines.push(fragments.join(""))
      lines.push("")
      break
    }
    default: {
      for (const [acteIndex, acte] of step.actes.entries()) {
        if (acteIndex !== 0 || !step.skipRootActe) {
          const libelleActe = acte.libelleActe
          const libelle = (libelleActe.libelleCourt || libelleActe.nomCanonique).replace(
            /\.$/,
            "",
          )
          const fragments = [libelle]
          if (acte.casSaisine !== undefined) {
            fragments.push(
              emojizedText(
                emojiFromCodierLibelle(acte.casSaisine.libelle),
                acte.casSaisine.libelle,
              ),
            )
          }
          if (acte.decision !== undefined) {
            fragments.push(
              emojizedText(
                emojiFromCodierLibelle(acte.decision.libelle),
                acte.decision.libelle,
              ),
            )
          }
          if (acte.statutAdoption !== undefined) {
            fragments.push(
              emojizedText(
                emojiFromCodierLibelle(acte.statutAdoption.libelle),
                acte.statutAdoption.libelle,
              ),
            )
          }
          if (acte.statutConclusion !== undefined) {
            fragments.push(
              emojizedText(
                emojiFromCodierLibelle(acte.statutConclusion.libelle),
                acte.statutConclusion.libelle,
              ),
            )
          }
          if (acte.typeDeclaration !== undefined) {
            fragments.push(
              emojizedText(
                emojiFromCodierLibelle(acte.typeDeclaration.libelle),
                acte.typeDeclaration.libelle,
              ),
            )
          }
          if (acte.typeMotion !== undefined) {
            fragments.push(
              emojizedText(
                emojiFromCodierLibelle(acte.typeMotion.libelle),
                acte.typeMotion.libelle,
              ),
            )
          }
          if (acte.typeMotionCensure !== undefined) {
            fragments.push(
              emojizedText(
                emojiFromCodierLibelle(acte.typeMotionCensure.libelle),
                acte.typeMotionCensure.libelle,
              ),
            )
          }
          if (acte.time !== undefined) {
            fragments.push("à", acte.time)
          }
          lines.push(fragments.join(" "))
          lines.push("")
        }
      }
    }
  }
}
