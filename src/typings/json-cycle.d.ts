declare module "json-cycle" {
  import jsonCycle from "json-cycle"

  export function decycle(object: any): any;
  export function parse($: any, reviver?: any): any;
  export function retrocycle($: any): any;
  export function stringify(object: any, replacer?: any, space?: any): any;
}
