import {
  ActeLegislatif,
  capitalizeFirstLetter,
  Document,
  DocumentUrlFormat,
  TypeActeLegislatif,
  urlFromDocument,
} from "@tricoteuses/assemblee"
import { insertDossierReferences } from "@tricoteuses/assemblee/lib/inserters"
import { insertLoiReferences } from "@tricoteuses/senat/lib/inserters"
import assert from "assert"
import commandLineArgs from "command-line-args"
import fs from "fs-extra"
import * as git from "isomorphic-git"
import jsonCycle from "json-cycle"
import fetch from "node-fetch"
import path from "path"
import querystring from "querystring"
import windows1252 from "windows-1252"

import config from "../config"
import { Dossier, DossierAndLoi, Loi } from "../types"
import { validateJsonResponse } from "../validators/responses"

interface TexteInfos {
  acteKey: string
  isBillDraft: boolean
  texte: Document
}

const dossierUidRegex = /^DL(R\d+)(L\d+)N(\d+)$/
const { gitlab } = config
const gitlabGroupByPath: { [groupPath: string]: any } = {}
const senatCheminRegExp = /^https?:\/\/www\.senat\.fr\/dossier-legislatif\/(.+)\.html$/
const urlAnRegExps = [
  /^https?:\/\/www\.assemblee-nationale\.fr\/\/?([0-9]+)\/dossiers\/(.+)\.asp(#.*)?$/,
  /^https?:\/\/www\.assemblee-nationale\.fr\/\/?dyn\/([0-9]+)\/dossiers\/(.+?)(#.*)?$/,
]

const optionsDefinitions = [
  {
    alias: "c",
    help: "commit files",
    name: "commit",
    type: Boolean,
  },
  {
    alias: "d",
    help: "only upsert the given bill (using dossier.titreDossier.titreChemin)",
    name: "dossier",
    type: String,
  },
  {
    alias: "p",
    help: "push changes",
    name: "push",
    type: Boolean,
  },
  {
    alias: "s",
    help: "don't log anything",
    name: "silent",
    type: Boolean,
  },
  {
    alias: "v",
    help: "verbose logs",
    name: "verbose",
    type: Boolean,
  },
  {
    defaultOption: true,
    help: 'directory containing a directory for each "dossier législatif"',
    name: "dossiersDir",
    type: String,
  },
]
const options = commandLineArgs(optionsDefinitions)

async function createGitlabProject(group: any, projectPath: string, projectName: string) {
  projectName = projectName.trim().replace(/'/g, " ")
  const requestUrl = new URL("api/v4/projects", gitlab.url).toString()
  const response = await fetch(requestUrl, {
    body: JSON.stringify(
      {
        // description:
        issues_enabled: false,
        jobs_enabled: false,
        merge_requests_enabled: false,
        name: projectName,
        namespace_id: group.id,
        path: projectPath,
        snippets_enabled: false,
        visibility: "public",
        wiki_enabled: false,
      },
      null,
      2,
    ),
    headers: {
      "Content-Type": "application/json; charset=utf-8",
      "Private-Token": gitlab.accessToken,
    },
    method: "POST",
  })
  if (!response.ok) {
    if (response.status === 400) {
      // Project already exists.
      // Caution: It doesn't return the existing project, but:
      // {
      //   "message": {
      //     "name": [
      //       "has already been taken"
      //     ],
      //     "path": [
      //       "has already been taken"
      //     ],
      //     "limit_reached": []
      //   }
      // }
      return null
    }
    const responseBody = await response.text()
    throw new Error(
      `Error while calling ${requestUrl}\n\nError ${response.status} ${response.statusText}:\n${responseBody}`,
    )
  }
  return await response.json()
}

/// Retrieve GitLab group containing repositories of "dossiers législatifs".
async function getGitlabGroup(groupPath: string) {
  let group = gitlabGroupByPath[groupPath]
  if (group !== undefined) {
    return group
  }
  const requestUrl = new URL(
    `api/v4/groups/${encodeURIComponent(groupPath)}`,
    gitlab.url,
  ).toString()
  const response = await fetch(requestUrl, {
    headers: {
      "Private-Token": gitlab.accessToken,
    },
    method: "GET",
  })
  if (!response.ok) {
    const responseBody = await response.text()
    throw new Error(
      `Error while calling ${requestUrl}\n\nError ${response.status} ${response.statusText}:\n${responseBody}`,
    )
  }
  group = await response.json()
  gitlabGroupByPath[groupPath] = group
  return group
}

async function getGitlabProject(group: any, projectPath: string) {
  const requestUrl = new URL(
    `api/v4/projects/${encodeURIComponent([group.full_path, projectPath].join("/"))}`,
    gitlab.url,
  ).toString()
  const response = await fetch(requestUrl, {
    headers: {
      "Private-Token": gitlab.accessToken,
    },
    method: "GET",
  })
  if (!response.ok) {
    if (response.status === 404) {
      // Project doesn't exist.
      return null
    }
    const responseBody = await response.text()
    throw new Error(
      `Error while calling ${requestUrl}\n\nError ${response.status} ${response.statusText}:\n${responseBody}`,
    )
  }
  return await response.json()
}

async function listGitlabProjectedBranches(group: any, projectPath: string) {
  const requestUrl = new URL(
    `api/v4/projects/${encodeURIComponent(
      [group.full_path, projectPath].join("/"),
    )}/protected_branches`,
    gitlab.url,
  ).toString()
  const response = await fetch(requestUrl, {
    headers: {
      "Private-Token": gitlab.accessToken,
    },
    method: "GET",
  })
  if (!response.ok) {
    const responseBody = await response.text()
    throw new Error(
      `Error while deleting branches at ${requestUrl}\n\nError ${response.status} ${response.statusText}:\n${responseBody}`,
    )
  }
  return await response.json()
}

async function main() {
  if (!options.silent) {
    console.log("Retrieving Assemblée dossiers…")
  }
  const dossiers: Dossier[] = []
  {
    let url = new URL(
      "dossiers?" +
        querystring.stringify({
          follow: [
            "acte.organe",
            "acte.provenance",
            "acte.rapporteurs.acteur",
            "acte.reunion",
            "acte.texteAdopte",
            "acte.texteAssocie",
            "acte.textesAssocies.texteAssocie",
            "acte.votes",
            "dossier.initiateur.acteurs.acteur",
            "dossier.initiateur.organe",
          ],
          legislature: 15,
          limit: 100,
        }),
      config.assembleeApiUrl,
    ).toString()
    while (url !== null) {
      if (options.verbose) {
        console.log(`  Retrieving dossiers from ${url}…`)
      }
      const response = await fetch(url)
      const [result, error] = await validateJsonResponse(null)(response)
      assert.strictEqual(error, null)
      for (const uid of result.uids) {
        const dossier = result.dossierByUid[uid]
        assert.notStrictEqual(dossier, undefined)
        insertDossierReferences(dossier, result, new Set())
        dossiers.push(dossier)
      }
      url = result.next
    }
  }

  if (!options.silent) {
    console.log("Retrieving Sénat lois…")
  }
  const lois: Loi[] = []
  {
    let url = new URL(
      "dossiers?" +
        querystring.stringify({
          follow: [
            "aud.org",
            "auteur.qua",
            "dateSeance.debat",
            "dateSeance.scrs",
            "docatt.rec",
            "docatt.typatt",
            "ecr.aut",
            "lecass.ass",
            "lecass.auds",
            "lecass.datesSeances",
            "lecass.debats",
            "lecass.lecassraps",
            "lecass.org",
            "lecass.textes",
            "lecassrap.rap",
            "lecture.lecasss",
            "lecture.typlec",
            "loi.deccoc",
            "loi.etaloi",
            "loi.lectures",
            "loi.typloi",
            "rap.denrap",
            "rap.docatts",
            "rap.ecrs",
            "rap.orgs",
            "texte.ecrs",
            "texte.org",
            "texte.oritxt",
          ],
          limit: 100,
          session: 2017,
        }),
      config.senatApiUrl,
    ).toString()
    while (url !== null) {
      if (options.verbose) {
        console.log(`  Retrieving lois from ${url}…`)
      }
      const response = await fetch(url)
      const [result, error] = await validateJsonResponse(null)(response)
      assert.strictEqual(error, null)
      for (const id of result.ids) {
        const loi = result.loi[id]
        if (loi !== undefined) {
          insertLoiReferences(loi, result, {})
          lois.push(loi)
        }
      }
      url = result.next
    }
  }

  const dossiersAndLois = mergeDossiersAndLois(dossiers, lois)

  const dossiersDir = options.dossiersDir
  fs.ensureDirSync(dossiersDir)
  const filteredDossiersAndLois = options.dossier
    ? dossiersAndLois.filter(
        ({ dossier }) =>
          dossier !== undefined && dossier.titreDossier.titreChemin === options.dossier,
      )
    : dossiersAndLois.slice(0, 4)
  for (const { dossier, loi } of filteredDossiersAndLois) {
    let dossierDir: string | undefined
    let dossierDirName: string | undefined
    let dossierName: string | undefined
    let gitlabGroupPath: string | undefined
    if (dossier !== undefined) {
      const match = dossierUidRegex.exec(dossier.uid)
      assert.notStrictEqual(
        match,
        null,
        `Unexpected structure for dossier parlementaire UID: ${dossier.uid}`,
      )
      const legislatureDirName = match![1] + match![2]
      dossierDirName = dossier.titreDossier.titreChemin || "N" + match![3]
      dossierDir = path.join(dossiersDir, legislatureDirName, dossierDirName)
      dossierName = dossier.titreDossier.titre
      gitlabGroupPath = [gitlab.groupPath, legislatureDirName].join("/")
    } else {
      assert.notStrictEqual(loi, undefined)
      dossierDirName = loi!.signet === null ? loi!.loicod : loi!.signet
      dossierDir = path.join(dossiersDir, dossierDirName)
      dossierName = [loi!.loient, loi!.motclef].filter(Boolean).join(" ")
      gitlabGroupPath = gitlab.groupPath
    }
    fs.ensureDirSync(dossierDir)

    if (options.commit) {
      if (!fs.existsSync(path.join(dossierDir, ".git"))) {
        await git.init({ dir: dossierDir })
      }
      await git.config({
        dir: dossierDir,
        path: "user.name",
        value: gitlab.username,
      })
      await git.config({
        dir: dossierDir,
        path: "user.email",
        value: gitlab.userEmail,
      })
      // Create GitLab project if it doesn't exist yet.
      const gitlabGroup = await getGitlabGroup(gitlabGroupPath!)
      await upsertGitlabProject(gitlabGroup, dossierDirName, dossierName)
      const protectedBranches = await listGitlabProjectedBranches(
        gitlabGroup,
        dossierDirName,
      )
      for (const protectedBranch of protectedBranches) {
        await unprotectGitlabProjectBranch(
          gitlabGroup,
          dossierDirName,
          protectedBranch.name,
        )
      }
      await git.addRemote({
        dir: dossierDir,
        remote: "origin",
        url: new URL([gitlabGroupPath, dossierDirName].join("/"), gitlab.url).toString(),
      })

      const currentBranch = await git.currentBranch({ dir: dossierDir })
      if (currentBranch !== "pages_originales") {
        const branches = await git.listBranches({ dir: dossierDir })
        if (branches.includes("pages_originales")) {
          await git.checkout({ dir: dossierDir, ref: "pages_originales" })
        } else {
          await git.branch({ checkout: true, dir: dossierDir, ref: "pages_originales" })
        }
      }
    }

    if (dossier !== undefined) {
      dossier.tricot = { textesMetadata: [] }

      const assembleeDir = path.join(dossierDir, "assemblee")
      fs.ensureDirSync(assembleeDir)

      const textesInfos: TexteInfos[] = []
      for (const acte of dossier.actesLegislatifs) {
        textesFromActes(acte, textesInfos)
      }

      for (const { acteKey, isBillDraft, texte } of textesInfos) {
        if (texte.uid.substr(4, 2) === "SN") {
          // Ignore documents originating from Sénat.
          if (options.verbose) {
            console.log(
              `Ignoring Sénat ${acteKey} ${texte.titres.titrePrincipal} (${texte.uid}).`,
            )
          }
          continue
        }
        let pageBuffer = null
        let url: string | null = null
        for (const format of [
          DocumentUrlFormat.RawHtml,
          DocumentUrlFormat.Html,
          DocumentUrlFormat.Pdf,
        ]) {
          url = urlFromDocument(texte, format)
          if (url === null) {
            continue
          }
          if (url.match(/https?:\/\/www\.senat\.fr\//) !== null) {
            if (!options.silent) {
              console.log(`Ignoring Sénat "texte" at ${url} in Assemblée dossier.`)
            }
            continue
          }
          if (!options.silent) {
            console.log(`Retrieving ${acteKey} ${texte.titres.titrePrincipal} at ${url}…`)
          }

          const response = await fetch(url)
          if (!response.ok) {
            if (response.status !== 404) {
              console.error(
                `Error while getting page "${url}" (uid: ${
                  texte.uid
                }):\n\nError:\n${JSON.stringify(
                  { code: response.status, message: response.statusText },
                  null,
                  2,
                )}`,
              )
            } else if (!options.silent) {
              console.log(`  Retrieval failed: ${response.status} ${response.statusText}`)
            }
            continue
          }
          pageBuffer = Buffer.from(new Uint8Array(await response.arrayBuffer()))
          if (format === DocumentUrlFormat.Html) {
            const page = windows1252.decode(pageBuffer.toString("binary"), {
              mode: "fatal",
            })
            if (page.includes("n'est pas encore édité")) {
              if (!options.silent) {
                console.log(`  Retrieval failed: HTML-edited version not available`)
              }
              pageBuffer = null
              continue
            }
          }
          break
        }
        if (pageBuffer === null) {
          continue
        }
        const urlSplit = url!.split("/")
        const pageFilename = urlSplit[urlSplit.length - 1]
        fs.writeFileSync(path.join(assembleeDir, pageFilename), pageBuffer)
        dossier.tricot.textesMetadata.push({
          filename: pageFilename,
          id: texte.uid,
          isBillDraft,
          title: capitalizeFirstLetter(texte.titres.titrePrincipal),
          url: url!,
        })
      }

      fs.writeFileSync(
        path.join(assembleeDir, "dossier.json"),
        jsonCycle.stringify(dossier, null, 2),
      )
    }
    if (loi !== undefined) {
      loi.tricot = { textesMetadata: [] }

      const senatDir = path.join(dossierDir, "senat")
      fs.ensureDirSync(senatDir)
      for (const lecture of loi.lectures || []) {
        for (const lecass of lecture.lecasss || []) {
          for (const texte of lecass.textes || []) {
            if (texte.texurl === null) {
              continue
            }
            if (
              texte.texurl.match(/https?:\/\/www2?\.assemblee-nationale\.fr\//) !== null
            ) {
              if (options.verbose) {
                console.log(`Ignoring Assemblée "texte" at ${texte.texurl} in Sénat loi.`)
              }
              continue
            }
            const url = new URL(texte.texurl, "http://www.senat.fr/leg/").toString()
            if (!options.silent) {
              console.log(`Retrieving texte n° ${texte.texnum} at ${url}…`)
            }

            const response = await fetch(url)
            const pageBuffer = Buffer.from(new Uint8Array(await response.arrayBuffer()))
            if (!response.ok) {
              if (response.status !== 404) {
                console.error(
                  `Error while getting page "${url}" (texnum: ${
                    texte.texnum
                  }):\n\nError:\n${JSON.stringify(
                    { code: response.status, message: response.statusText },
                    null,
                    2,
                  )}`,
                )
              } else if (!options.silent) {
                console.log(
                  `  Retrieval failed: ${response.status} ${response.statusText}`,
                )
              }
              continue
            }
            const urlSplit = url.split("/")
            const pageFilename = urlSplit[urlSplit.length - 1]
            fs.writeFileSync(path.join(senatDir, pageFilename), pageBuffer)
            loi.tricot.textesMetadata.push({
              filename: pageFilename,
              id: texte.texcod,
              isBillDraft: true,
              title: `${capitalizeFirstLetter(
                loi.typloi!.typloilib,
              )} ${lecass.loiintmod || loi.loiintori}`,
              url,
            })
          }
        }
      }

      fs.writeFileSync(
        path.join(senatDir, "dossier.json"),
        jsonCycle.stringify(loi, null, 2),
      )
    }

    if (options.commit) {
      let commitNeeded = false
      const statusMatrix = await git.statusMatrix({
        dir: dossierDir,
      })
      for (const [
        filepath,
        headStatus,
        workTreeStatus,
        // stateStatus,
      ] of statusMatrix) {
        if (workTreeStatus === 0) {
          if (headStatus === 1) {
            // File deleted
            await git.remove({
              dir: dossierDir,
              filepath,
            })
            commitNeeded = true
          }
        } else if (workTreeStatus === 2) {
          // File added or modified
          await git.add({
            dir: dossierDir,
            filepath,
          })
          commitNeeded = true
        }
      }
      if (commitNeeded) {
        await git.commit({
          dir: dossierDir,
          message: "Nouvelle moisson",
          // ref: "pages_originales",
        })
        if (options.push) {
          const pushResponse = await git.push({
            dir: dossierDir,
            oauth2format: "gitlab",
            // ref: "pages_originales",
            remote: "origin",
            token: gitlab.accessToken,
          })
          assert.strictEqual(pushResponse.errors, undefined)
        }
      }
    }
  }
}

function mergeDossiersAndLois(dossiers: Dossier[], lois: Loi[]): DossierAndLoi[] {
  const dossierBySignetSenat: { [signetSenat: string]: Dossier } = {}
  const dossierByTitreCheminByLegislature: {
    [legislature: string]: { [titreChemin: string]: Dossier }
  } = {}
  const signetSenatByDossierUid: { [uid: string]: string } = {}
  for (const dossier of dossiers) {
    const titreDossier = dossier.titreDossier
    if (titreDossier.senatChemin !== undefined) {
      const match = titreDossier.senatChemin.match(senatCheminRegExp)
      assert.notStrictEqual(
        match,
        null,
        `Invalid senatChemin: ${titreDossier.senatChemin}`,
      )
      const signetSenat = match![1]
      signetSenatByDossierUid[dossier.uid] = signetSenat
      dossierBySignetSenat[signetSenat] = dossier
    }
    if (titreDossier.titreChemin !== undefined) {
      let dossierByTitreChemin = dossierByTitreCheminByLegislature[dossier.legislature]
      if (dossierByTitreChemin === undefined) {
        dossierByTitreChemin = dossierByTitreCheminByLegislature[dossier.legislature] = {}
      }
      dossierByTitreChemin[titreDossier.titreChemin] = dossier
    }
  }

  // const legislatureByLoicod: { [loicod: string]: string} = {}
  const loiBySignet: { [signet: string]: Loi } = {}
  const loisByTitreCheminByLegislatureAssemblee: {
    [legislature: string]: { [titreChemin: string]: Loi[] }
  } = {}
  // const titreCheminByLoicod: { [loicod: string]: string} = {}
  for (const loi of lois) {
    if (loi.signet !== null) {
      loiBySignet[loi.signet] = loi
    }
    if (loi.url_an !== null) {
      let match = null
      for (const urlAnRegExp of urlAnRegExps) {
        match = loi.url_an.match(urlAnRegExp)
        if (match !== null) {
          break
        }
      }
      assert.notStrictEqual(match, null, `Invalid loi.url_an: ${loi.url_an}`)
      const legislature = match![1]
      const titreChemin = match![2]
      let loisByTitreChemin = loisByTitreCheminByLegislatureAssemblee[legislature]
      if (loisByTitreChemin === undefined) {
        loisByTitreChemin = loisByTitreCheminByLegislatureAssemblee[legislature] = {}
      }
      // legislatureByLoicod[loi.loicod] = legislature
      // titreCheminByLoicod[loi.loicod] = titreChemin
      let loisAtTitreChemin = loisByTitreChemin[titreChemin]
      if (loisAtTitreChemin === undefined) {
        loisAtTitreChemin = loisByTitreChemin[titreChemin] = [loi]
      } else {
        loisAtTitreChemin.push(loi)
      }
    }
  }

  const dossiersAndLois: DossierAndLoi[] = []
  const loicodsAttachedToDossiers = new Set()
  for (const dossier of dossiers) {
    let loi: Loi | null = null
    const signetSenat = signetSenatByDossierUid[dossier.uid]
    if (signetSenat !== undefined) {
      const loiFound = loiBySignet[signetSenat]
      if (loiFound !== undefined) {
        loi = loiFound
        loicodsAttachedToDossiers.add(loi.loicod)
      }
    }
    const titreDossier = dossier.titreDossier
    if (titreDossier.titreChemin !== undefined) {
      const loisByTitreChemin =
        loisByTitreCheminByLegislatureAssemblee[dossier.legislature]
      if (loisByTitreChemin !== undefined) {
        const loisAtTitreChemin = loisByTitreChemin[titreDossier.titreChemin]
        if (loisAtTitreChemin !== undefined) {
          if (loi === null) {
            // TODO: When more than one "loi" have the same name, we need to
            // differentiate them (by type or year or...)
            assert.strictEqual(loisAtTitreChemin.length, 1)
            loi = loisAtTitreChemin[0]
            loicodsAttachedToDossiers.add(loi.loicod)
          } else {
            assert(
              loisAtTitreChemin.some(
                loiAtTitreChemin => loiAtTitreChemin.loicod === loi!.loicod,
              ),
            )
          }
        }
      }
    }
    dossiersAndLois.push({
      dossier,
      // TODO first: "assemblee" ou "senat",
      loi: loi === null ? undefined : loi,
    })
  }
  for (const loi of lois) {
    if (!loicodsAttachedToDossiers.has(loi.loicod)) {
      dossiersAndLois.push({
        // dossier: undefined,
        // first: "senat",
        loi,
      })
    }
  }
  return dossiersAndLois
}

function textesFromActes(acte: ActeLegislatif, textesInfos: TexteInfos[]) {
  if (acte.texteAdopte !== undefined) {
    assert.strictEqual(acte.texteAdopte.xsiType, "texteLoi_Type")
    const texteInfos = {
      acteKey: "texteAdopte",
      isBillDraft: acte.xsiType === TypeActeLegislatif.DepotRapportType,
      texte: acte.texteAdopte,
    }
    textesInfos.push(texteInfos)
  }
  if (acte.texteAssocie) {
    if (acte.texteAssocie.xsiType === "texteLoi_Type") {
      const texteInfos = {
        acteKey: "texteAssocie",
        isBillDraft: [
          TypeActeLegislatif.DepotInitiativeNavetteType,
          TypeActeLegislatif.DepotInitiativeType,
          TypeActeLegislatif.DepotLettreRectificativeType,
        ].includes(acte.xsiType),
        texte: acte.texteAssocie,
      }
      textesInfos.push(texteInfos)
    }
  }

  if (acte.actesLegislatifs !== undefined) {
    for (const childActe of acte.actesLegislatifs) {
      textesFromActes(childActe, textesInfos)
    }
  }
}

async function unprotectGitlabProjectBranch(
  group: any,
  projectPath: string,
  branchName: string,
) {
  const requestUrl = new URL(
    `api/v4/projects/${encodeURIComponent(
      [group.full_path, projectPath].join("/"),
    )}/protected_branches/${branchName}`,
    gitlab.url,
  ).toString()
  const response = await fetch(requestUrl, {
    headers: {
      "Private-Token": gitlab.accessToken,
    },
    method: "DELETE",
  })
  if (!response.ok) {
    const responseBody = await response.text()
    throw new Error(
      `Error while deleting branches at ${requestUrl}\n\nError ${response.status} ${response.statusText}:\n${responseBody}`,
    )
  }
  // return await response.json()
}

/// Try to create a GitLab project (don't fail if project already exists).
async function updateGitlabProject(group: any, projectPath: string, projectName: string) {
  projectName = projectName.trim().replace(/'/g, " ")
  const requestUrl = new URL(
    `api/v4/projects/${encodeURIComponent([group.full_path, projectPath].join("/"))}`,
    gitlab.url,
  ).toString()
  const response = await fetch(requestUrl, {
    body: JSON.stringify(
      {
        // description:
        issues_enabled: false,
        jobs_enabled: false,
        merge_requests_enabled: false,
        name: projectName,
        // namespace_id: group.id,
        // path: projectPath,
        snippets_enabled: false,
        visibility: "public",
        wiki_enabled: false,
      },
      null,
      2,
    ),
    headers: {
      "Content-Type": "application/json; charset=utf-8",
      "Private-Token": gitlab.accessToken,
    },
    method: "PUT",
  })
  if (!response.ok) {
    const responseBody = await response.text()
    throw new Error(
      `Error while calling ${requestUrl}\n\nError ${response.status} ${response.statusText}:\n${responseBody}`,
    )
  }
  return await response.json()
}

async function upsertGitlabProject(group: any, projectPath: string, projectName: string) {
  projectName = projectName.trim().replace(/'/g, " ")
  const project = await getGitlabProject(group, projectPath)
  if (project === null) {
    return await createGitlabProject(group, projectPath, projectName)
  }
  return await updateGitlabProject(group, projectPath, projectName)
}

git.plugins.set("fs", fs)

main().catch(error => {
  console.log(error)
  process.exit(1)
})
