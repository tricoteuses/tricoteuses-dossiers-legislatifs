require("dotenv").config()

import { validateConfig } from "./validators/config"

const config = {
  assembleeApiUrl: process.env.TRICOTEUSES_DOSSIERS_LEGISLATIFS_ASSEMBLEE_API_URL,
  gitlab: {
    accessToken: process.env.TRICOTEUSES_DOSSIERS_LEGISLATIFS_GITLAB_ACCESS_TOKEN,
    groupPath: process.env.TRICOTEUSES_DOSSIERS_LEGISLATIFS_GITLAB_GROUP_PATH,
    url: process.env.TRICOTEUSES_DOSSIERS_LEGISLATIFS_GITLAB_URL,
    userEmail: process.env.TRICOTEUSES_DOSSIERS_LEGISLATIFS_GITLAB_USER_EMAIL,
    username: process.env.TRICOTEUSES_DOSSIERS_LEGISLATIFS_GITLAB_USERNAME,
  },
  senatApiUrl: process.env.TRICOTEUSES_DOSSIERS_LEGISLATIFS_SENAT_API_URL,
  uiUrl: process.env.TRICOTEUSES_DOSSIERS_LEGISLATIFS_UI_URL,
}

const [validConfig, error] = validateConfig(config)
if (error !== null) {
  console.error(
    `Error in  configuration:\n${JSON.stringify(
      validConfig,
      null,
      2,
    )}\nError:\n${JSON.stringify(error, null, 2)}`,
  )
  process.exit(-1)
}

export default validConfig
