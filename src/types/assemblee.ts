import {
  ActeLegislatif,
  TypeActeLegislatif,
  Document,
  DossierParlementaire,
  InfoJo,
} from "@tricoteuses/assemblee"

import { StepCodeTypeOrgane, Tricot } from "./shared"

export interface Dossier extends DossierParlementaire {
  tricot?: Tricot
}

export interface AssembleeStep {
  actes: StepActe[]
  date?: Date
  index: number
  infoJo?: InfoJo
  leafActe: StepActe
  organeCodeType: StepCodeTypeOrgane
  organeName: string
  originOrganeCodeType?: StepCodeTypeOrgane
  originOrganeName?: string
  rootActe: StepActe
  skipRootActe: boolean
  texteLoi?: Document
  title?: string
  typeActe: StepTypeActe
}

export interface StepActe extends ActeLegislatif {
  // actesLegislatifs is always undefined.
  time?: string // time extracted from date.date, when it is meaningful.
}

export enum StepOnlyTypeActe {
  EtudeImpactContributionInternauteOuverture = "EtudeImpact_Type.contributionInternaute.dateOuverture",
  EtudeImpactContributionInternauteFermeture = "EtudeImpact_Type.contributionInternaute.dateFermeture",
  PromulgationInfoJo = "Promulgation.infoJo",
  PromulgationInfoJoRect = "Promulgation.infoJoRect",
}

export type StepTypeActe = TypeActeLegislatif | StepOnlyTypeActe
