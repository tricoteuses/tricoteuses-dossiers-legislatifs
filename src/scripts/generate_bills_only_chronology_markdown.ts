import { TypeActeLegislatif } from "@tricoteuses/assemblee"
import assert from "assert"
import commandLineArgs from "command-line-args"
import fs from "fs-extra"
import jsonCycle from "json-cycle"
import path from "path"

import {
  chronologyFromAssembleeSteps,
  chronologyFromSenatSteps,
  filterAuthoritativeAssembleeStep,
  filterAuthoritativeSenatStep,
  mergeAssembleeAndSenatChronologies,
  stepsFromDossier,
  stepsFromLoi,
} from "../chronologies"
import { markdownLinesFromChronology } from "../markdown"
import {
  AssembleeStep,
  DateBlock,
  Dossier,
  Loi,
  SenatStep,
  SenatStepKind,
  StepTypeActe,
} from "../types"

const optionsDefinitions = [
  {
    alias: "s",
    help: "don't log anything",
    name: "silent",
    type: Boolean,
  },
  {
    alias: "v",
    help: "verbose logs",
    name: "verbose",
    type: Boolean,
  },
  {
    defaultOption: true,
    help: 'directory of a "dossier législatif"',
    name: "dossierDir",
    type: String,
  },
]
const options = commandLineArgs(optionsDefinitions)

async function main() {
  const dossierDir = options.dossierDir
  assert(fs.statSync(dossierDir).isDirectory(), `${dossierDir} is not a directory`)

  if (!options.silent) {
    console.log(`Generating README file for ${dossierDir}…`)
  }

  let assembleeChronology: DateBlock[] | null = null
  const assembleeDir = path.join(dossierDir, "assemblee")
  let dossier: Dossier | null = null
  if (fs.statSync(assembleeDir).isDirectory()) {
    const dossierFilePath = path.join(assembleeDir, "dossier.json")
    const dossierJson = fs.readFileSync(dossierFilePath, {
      encoding: "utf8",
    })
    dossier = jsonCycle.parse(dossierJson)
    const steps = stepsFromDossier(dossier!)
      .filter(filterAuthoritativeAssembleeStep)
      .filter((step: AssembleeStep) => {
        if (!([
          TypeActeLegislatif.DepotInitiativeNavetteType,
          TypeActeLegislatif.DepotInitiativeType,
          TypeActeLegislatif.DepotLettreRectificativeType,
          TypeActeLegislatif.DepotRapportType,
        ] as StepTypeActe[]).includes(step.typeActe)) {
          return false
        }
        if (step.typeActe === TypeActeLegislatif.DepotRapportType && step.leafActe.texteAdopte === undefined) {
          // Ignore "rapports" without "texte adopté".
          return false
        }
        return true
      }).map((step: AssembleeStep) => {
        if (step.typeActe === TypeActeLegislatif.DepotRapportType && step.leafActe.texteAssocie !== undefined) {
          // Keep only "texte adopté" in "rapports".
          delete step.leafActe.texteAssocie
        }
        return step
      })
    assembleeChronology = chronologyFromAssembleeSteps(steps)
  }
  fs.writeFileSync(
    path.join(dossierDir, "chronologie_assemblee_textes_seuls.json"),
    jsonCycle.stringify(assembleeChronology, null, 2),
  )

  let loi: Loi | null = null
  let senatChronology: DateBlock[] | null = null
  const senatDir = path.join(dossierDir, "senat")
  if (fs.statSync(senatDir).isDirectory()) {
    const loiFilePath = path.join(senatDir, "dossier.json")
    const loiJson = fs.readFileSync(loiFilePath, {
      encoding: "utf8",
    })
    loi = jsonCycle.parse(loiJson)
    const steps = stepsFromLoi(loi!)
      .filter(filterAuthoritativeSenatStep)
      .filter((step: SenatStep) => step.kind === SenatStepKind.Texte)
    senatChronology = chronologyFromSenatSteps(steps)
  }
  fs.writeFileSync(
    path.join(dossierDir, "chronologie_senat_textes_seuls.json"),
    jsonCycle.stringify(senatChronology, null, 2),
  )

  const chronology = mergeAssembleeAndSenatChronologies(
    assembleeChronology,
    senatChronology,
  )
  fs.writeFileSync(
    path.join(dossierDir, "chronologie_textes_seuls.json"),
    jsonCycle.stringify(chronology, null, 2),
  )

  const lines = markdownLinesFromChronology(chronology, dossier, loi)
  fs.writeFileSync(path.join(dossierDir, "README_textes_seuls.md"), lines.join("\n"))
}

main().catch(error => {
  console.log(error)
  process.exit(1)
})
