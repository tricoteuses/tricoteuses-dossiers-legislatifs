export {
  dateFromNullableDateOrIsoString,
  localIso8601StringFromDate,
} from "@tricoteuses/assemblee"
import format from "date-fns/format"
import frLocale from "date-fns/locale/fr"
import parse from "date-fns/parse"
import parseISO from "date-fns/parseISO"

/// Convert strings like "2017-11-24+01:00" to dates.
export function dateFromYearMonthDayTimezoneString(dateString: string): Date {
  return parse(dateString, "yyyy-MM-ddXXX", new Date())
}

export function displayDateFromLocalIsoString(dateString: string): string {
  const date = parseISO(dateString)
  dateString = format(date, "EEEE d LLLL yyyy H'h'mm", { locale: frLocale })
  if (dateString.endsWith(" 0h00")) {
    dateString = dateString.substring(0, dateString.length - 5)
  }
  return dateString
}

export function frenchDateWithoutDayOfWeekFromDateOrString(
  dateOrString: Date | string,
): string {
  const date: Date = dateOrString instanceof Date ? dateOrString : parseISO(dateOrString)
  let dateString = format(date, "d LLLL yyyy H'h'mm", { locale: frLocale })
  if (dateString.endsWith(" 0h00")) {
    dateString = dateString.substring(0, dateString.length - 5)
  }
  return dateString
}

export function frenchTimeFromDateOrString(dateOrString: Date | string): string {
  const date: Date = dateOrString instanceof Date ? dateOrString : parseISO(dateOrString)
  return format(date, "H'h'mm", { locale: frLocale })
}
