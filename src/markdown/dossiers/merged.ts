import {
  capitalizeFirstLetter,
  shortNameFromOrgane,
  stateFromDossier,
} from "@tricoteuses/assemblee"

import { procedureAccelereeFromActes } from "../../chronologies"
import {
  displayDateFromLocalIsoString,
  frenchDateWithoutDayOfWeekFromDateOrString,
} from "../../dates"
import {
  emojiFromOrganeCodeType,
  emojiFromStatus,
  markdownLinesFromAssembleeStep,
} from "./assemblee"
import {
  emojiFromEtaloilib,
  markdownLinesFromSenatStep,
} from "./senat"
import { emojizedText } from "../shared"
import { DateBlock, Dossier, Loi } from "../../types"

export function markdownLinesFromChronology(
  chronology: DateBlock[],
  dossier: Dossier | null,
  loi: Loi | null,
): string[] {
  const lines = []

  // Title (status)
  {
    // For the title, prefer the Sénat version, because they handle diacritical marks.
    // For example: "Énergie et climat" instead of "Energie et climat"
    const title = loi === null ? dossier!.titreDossier.titre : loi.loient
    let stateColor
    let stateLabel
    if (dossier !== null) {
      let state = stateFromDossier(dossier)
      stateColor = emojiFromStatus(state.currentStatus)
      stateLabel = state.label
    } else {
      stateColor = emojiFromEtaloilib(loi!.etaloi!.etaloilib)
      stateLabel =
        loi!.etaloi!.etaloilib === "promulgué ou adopté (ppr)"
          ? "adopté"
          : loi!.etaloi!.etaloilib
    }
    lines.push(`# ${title} ${emojizedText(stateColor, `(${stateLabel})`)}`)

    const procedureParlementaire =
      dossier === null
        ? capitalizeFirstLetter(loi!.typloi!.typloiden!)
        : dossier.procedureParlementaire.libelle
    lines.push(`# _${procedureParlementaire}_`)

    lines.push("")
  }

  // Initiateurs
  if (dossier === null) {
    // TODO: Generate "initiateurs" for Sénat.
  } else {
    const initiateur = dossier.initiateur
    if (initiateur !== undefined) {
      const initiateurLines = []
      if (initiateur.acteurs !== undefined) {
        for (const acteur of initiateur.acteurs) {
          if (acteur.acteur !== undefined) {
            const ident = acteur.acteur.etatCivil.ident
            let acteurLine = `${ident.prenom} ${ident.nom}`
            const mandat = acteur.mandat
            if (mandat !== undefined) {
              const organes = mandat.organes
              if (organes !== undefined && organes.length > 0) {
                acteurLine += ", " + shortNameFromOrgane(organes[0])
              } else {
                const infosQualite = mandat.infosQualite
                acteurLine +=
                  ", " + (infosQualite.libQualiteSex || infosQualite.libQualite)
              }
            }
            initiateurLines.push(acteurLine)
          }
        }
      }
      const organe = initiateur.organe
      if (organe !== undefined) {
        initiateurLines.push(shortNameFromOrgane(organe))
      }
      if (initiateurLines.length === 1) {
        lines.push(`_Initiateur_ : ${initiateurLines[0]}`)
        lines.push("")
      } else if (initiateurLines.length > 1) {
        lines.push("_Initiateurs_ :")
        for (const line of initiateurLines) {
          lines.push(`* ${line}`)
        }
        lines.push("")
      }
    }
  }

  // Procédure accélérée
  if (dossier === null) {
    if (
      loi!.proaccdat !== null &&
      loi!.proaccoppdat === null &&
      loi!.retproaccdat === null
    ) {
      lines.push("Procédure accélérée engagée par le Gouvernement", "")
    }
  } else {
    const procedureAccelereeActe = procedureAccelereeFromActes(dossier.actesLegislatifs)
    if (procedureAccelereeActe !== null) {
      lines.push("Procédure accélérée engagée par le Gouvernement", "")
    }
  }

  for (const dateBlock of chronology) {
    // Day
    if (dateBlock.dateLocalIsoString === undefined) {
      lines.push("## _Date non précisée_", "")
    } else {
      lines.push(
        `## ${displayDateFromLocalIsoString(dateBlock.dateLocalIsoString.split("T")[0])}`,
        "",
      )
    }

    for (const organeBlockKey of dateBlock.keys) {
      const organeBlock = dateBlock.organeBlockByKey[organeBlockKey]
      const lineFragments = []
      if (organeBlock.originOrganeName !== undefined) {
        lineFragments.push(
          emojizedText(
            emojiFromOrganeCodeType(organeBlock.originOrganeCodeType!),
            organeBlock.originOrganeName,
          ),
          "→",
        )
      }
      let fragment = organeBlock.organeName
      if (organeBlock.title !== undefined) {
        fragment += ", " + organeBlock.title
      }
      lineFragments.push(
        emojizedText(emojiFromOrganeCodeType(organeBlock.organeCodeType!), fragment),
      )
      lines.push(`### ${lineFragments.join(" ")}`, "")

      for (const subOrganeBlockKey of organeBlock.keys) {
        const subOrganeBlock = organeBlock.subOrganeBlockByKey[subOrganeBlockKey]
        if (subOrganeBlock.title) {
          lines.push(`#### ${subOrganeBlock.title}`, "")
        }
        // TODO: Add "(saisie pour avis)" or "(saisie au fond)".

        if (subOrganeBlock.assembleeSteps !== undefined) {
          for (const assembleeStep of subOrganeBlock.assembleeSteps!) {
            markdownLinesFromAssembleeStep(lines, assembleeStep)
          }
        }

        if (subOrganeBlock.senatSteps !== undefined) {
          for (const senatStep of subOrganeBlock.senatSteps!) {
            markdownLinesFromSenatStep(lines, senatStep)
          }
        }
      }
    }
  }

  // "Pour en savoir plus" block
  lines.push("## Pour en savoir plus…")
  lines.push("")

  if (dossier !== null) {
    lines.push("### Assemblée nationale")
    lines.push("")
    lines.push(
      `[Dossier législatif : ${dossier.titreDossier.titre}](http://www.assemblee-nationale.fr/dyn/${dossier.legislature}/dossiers/${dossier.titreDossier.titreChemin})`,
    )
    lines.push("")
  }

  if (loi !== null && loi.typloi !== undefined) {
    lines.push("### Sénat")
    lines.push("")
    lines.push(
      `[Dossier législatif ${loi.typloi.typloide} ${loi.typloi.typloiden}](https://www.senat.fr/dossier-legislatif/${loi.signet}.html)`,
    )
    lines.push("")
  }

  if (loi !== null && loi.typloi !== undefined && loi.doscocurl !== null) {
    lines.push("### Conseil constitutionnel")
    lines.push("")
    lines.push(
      `[Dossier législatif ${loi.typloi.typloide} ${loi.typloi.typloiden}](${loi.doscocurl})`,
    )
    lines.push("")
  }

  if (loi !== null && loi.typloi !== undefined) {
    lines.push("### La fabrique de la loi")
    lines.push("")
    lines.push(
      `[Dossier législatif ${loi.typloi.typloide} ${loi.typloi.typloiden}](https://www.lafabriquedelaloi.fr/articles.html?loi=${loi.signet})`,
    )
    lines.push("")
  }

  if (loi !== null && loi.typloi !== undefined && loi.date_loi !== null) {
    const lastUrlJo = loi.url_jo3 || loi.url_jo2 || loi.url_jo
    const urlRegExp = /^https?:\/\/www\.legifrance\.gouv\.fr\/eli\//
    const archeoLexUrl =
      lastUrlJo === null || lastUrlJo.match(urlRegExp) === null
        ? null
        : lastUrlJo.replace(urlRegExp, "https://archeo-lex.fr/eli/")
    if (archeoLexUrl !== null) {
      lines.push("### Archéo Lex")
      lines.push("")
      lines.push(
        `[Historique de la loi n° ${
          loi.numero
        } du ${frenchDateWithoutDayOfWeekFromDateOrString(loi.date_loi)} ${
          loi.loititjo
        }](${archeoLexUrl})`,
      )
      lines.push("")
    }
  }

  return lines
}
