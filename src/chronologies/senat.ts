import { Lecture } from "@tricoteuses/senat"
import startOfDay from "date-fns/startOfDay"

import { dateFromNullableDateOrIsoString, localIso8601StringFromDate } from "../dates"
import {
  CodeTypeOrgane,
  DateBlock,
  LecAssSimple,
  LectureSimple,
  Loi,
  SenatStep,
  SenatStepKind,
  StepCodeTypeOrgane,
  StepOnlyCodeTypeOrgane,
} from "../types"

const stepPriorityByKind = {
  AmendementsCommission: 5,
  AmendementsSeance: 7,
  Aud: 6, // Réunions de commissions
  DateSeance: 8, // Séances publiques
  DecisionConseilConstitutionnel: 11,
  EngagementProcedureAcceleree: 1,
  LecAssRap: 4, // Les "rapports" are published before "réunions de commissions" or "séances publiques",
  OppositionProcedureAcceleree: 2,
  PromulgationLoi: 12,
  PublicationLoi: 13,
  PublicationPremierCorrectifLoi: 14,
  PublicationSecondCorrectifLoi: 15,
  RetraitProcedureAcceleree: 3,
  SaisineConseilConstitutionnel: 10,
  Texte: 9, // "Textes votés" published at the end of "réunions de commissions" or "séances publiques"
}

/// Group steps by date, organe & sub-organe.
export function chronologyFromSenatSteps(steps: SenatStep[]): DateBlock[] {
  let currentDateLocalIsoString = null
  let dateBlock: DateBlock | null = null
  const dateBlocks: DateBlock[] = []
  for (const step of steps) {
    const dateLocalIsoString =
      step.date === undefined ? undefined : localIso8601StringFromDate(step.date)
    if (dateLocalIsoString !== currentDateLocalIsoString) {
      currentDateLocalIsoString = dateLocalIsoString
      dateBlock = {
        dateLocalIsoString,
        keys: [],
        organeBlockByKey: {},
      }
      dateBlocks.push(dateBlock)
    }
    const organeBlockKey = `${step.originOrganeName || "zz"}$${step.organeName}$${
      step.title
    }`
    let organeBlock = dateBlock!.organeBlockByKey[organeBlockKey]
    if (organeBlock === undefined) {
      dateBlock!.keys.push(organeBlockKey)
      organeBlock = dateBlock!.organeBlockByKey[organeBlockKey] = {
        // id: step.leafActe.uid,
        keys: [],
        organeCodeType: step.organeCodeType,
        organeName: step.organeName,
        originOrganeCodeType: step.originOrganeCodeType,
        originOrganeName: step.originOrganeName,
        subOrganeBlockByKey: {},
        title: step.title,
      }
    }

    for (const subOrgane of step.subOrganes) {
      const subOrganeBlockKey =
        subOrgane === null ? "" : subOrgane.orglibaff || subOrgane.orgnom
      let subOrganeBlock = organeBlock.subOrganeBlockByKey[subOrganeBlockKey]
      if (subOrganeBlock === undefined) {
        if (subOrganeBlockKey) {
          organeBlock.keys.push(subOrganeBlockKey)
        } else {
          // The main sub-block must always be the first.
          organeBlock.keys.unshift(subOrganeBlockKey)
        }
        subOrganeBlock = organeBlock.subOrganeBlockByKey[subOrganeBlockKey] = {
          // codeActe: step.leafActe.codeActe,
          // id: step.leafActe.uid,
          senatSteps: [],
          title: subOrganeBlockKey,
        }
      }
      subOrganeBlock.senatSteps!.push(step)
    }
  }

  return dateBlocks
}

export function filterAuthoritativeSenatStep(step: SenatStep): boolean {
  switch (step.kind) {
    case SenatStepKind.LecAssRap: {
      const ass = step.lecass.ass
      return ass === undefined || ass.codass !== "A"
    }
    case SenatStepKind.Texte: {
      const ass = step.lecass.ass
      return ass === undefined || ass.codass !== "A"
    }
    default:
      return true
  }
}

export function senatTextesStepsFromLoi(loi: Loi): SenatStep[] {
    const steps = stepsFromLoi(loi)
      .filter(filterAuthoritativeSenatStep)
      .filter((step: SenatStep) => step.kind === SenatStepKind.Texte)
      return steps
}

function stepNumber(step: SenatStep): number {
  switch (step.kind) {
    case "Aud":
      return step.audcle
    case "DateSeance":
      return step.code
    case "LecAssRap":
      return step.rap!.rapnum || 0
    case "Texte":
      return step.texnum || 0
    default:
      return 0
  }
}

/// Convert events of loi to sequential steps.
export function stepsFromLoi(loi: Loi): SenatStep[] {
  const steps: SenatStep[] = []
  for (const lecture of loi.lectures || []) {
    for (const lecass of lecture.lecasss || []) {
      const organeCodeType: StepCodeTypeOrgane =
        ({
          A: CodeTypeOrgane.Assemblee,
          I: StepOnlyCodeTypeOrgane.Autre, // Indéterminé
          S: CodeTypeOrgane.Senat,
        } as { [key: string]: StepCodeTypeOrgane })[lecass.codass] ||
        StepOnlyCodeTypeOrgane.Autre
      const organeName: string =
        ({
          A: "Assemblée",
          I: "Indéterminé",
          S: "Sénat",
        } as { [key: string]: string })[lecass.codass] || "Indéterminé"
      if (lecass.auds !== undefined) {
        for (const aud of lecass.auds) {
          // Réunion de commission
          steps.push({
            ...aud,
            date: dateFromNullableDateOrIsoString(aud.auddat, undefined)!,
            index: -1, // Real index will be put later.
            kind: SenatStepKind.Aud,
            lecass,
            lecture,
            organeCodeType,
            organeName,
            subOrganes: [aud.org || null],
            title: stepTitleFromLecture(lecture),
          })
        }
      }
      if (lecass.datesSeances !== undefined) {
        for (const dateSeance of lecass.datesSeances) {
          steps.push({
            // Séance publique
            ...dateSeance,
            date: dateFromNullableDateOrIsoString(dateSeance.date_s, undefined)!,
            index: -1, // Real index will be put later.
            kind: SenatStepKind.DateSeance,
            lecass,
            lecture,
            organeCodeType,
            organeName,
            subOrganes: [null],
            title: stepTitleFromLecture(lecture),
          })
        }
      }
      if (lecass.lecassraps !== undefined) {
        for (const lecassrap of lecass.lecassraps) {
          steps.push({
            // Rapport
            ...lecassrap,
            date: dateFromNullableDateOrIsoString(lecassrap.rap!.date_depot, undefined)!,
            index: -1, // Real index will be put later.
            kind: SenatStepKind.LecAssRap,
            lecass,
            lecture,
            organeCodeType,
            organeName,
            subOrganes: lecassrap.rap!.orgs!,
            title: stepTitleFromLecture(lecture),
          })
        }
      }
      if (lecass.textes !== undefined) {
        for (const texte of lecass.textes) {
          steps.push({
            // Texte législatif (projet de loi, proposition de loi…)
            ...texte,
            date: dateFromNullableDateOrIsoString(texte.txtoritxtdat, undefined)!,
            index: -1, // Real index will be put later.
            kind: SenatStepKind.Texte,
            lecass,
            lecture,
            organeCodeType,
            organeName,
            subOrganes: [texte.org || null],
            title: stepTitleFromLecture(lecture),
          })
        }
      }
      if (lecass.lecassamecom !== null) {
        steps.push({
          // Amendements de commission
          ...lecass,
          date: startOfDay(
            dateFromNullableDateOrIsoString(lecass.lecassamecomdat, undefined)!,
          ),
          index: -1, // Real index will be put later.
          kind: SenatStepKind.AmendementsCommission,
          lecass,
          lecture,
          organeCodeType,
          organeName,
          subOrganes: [lecass.org || null],
          title: stepTitleFromLecture(lecture),
        })
      }
      if (lecass.lecassame !== null) {
        steps.push({
          ...lecass,
          date: startOfDay(
            dateFromNullableDateOrIsoString(lecass.lecassamedat, undefined)!,
          ),
          index: -1, // Real index will be put later.
          kind: SenatStepKind.AmendementsSeance,
          lecass,
          lecture,
          organeCodeType,
          organeName,
          subOrganes: [null],
          title: stepTitleFromLecture(lecture),
        })
      }
    }
  }

  if (loi.proaccdat !== null) {
    // Procédure accélérée
    const lecass: LecAssSimple = {
      ass: {
        codass: "G",
        libass: "Gouvernement",
      },
      lecassidt: "procedure_acceleree",
    }
    const lecture: LectureSimple = {
      lecasss: [lecass],
      leccom: "Gouvernement",
    }
    const organeCodeType = CodeTypeOrgane.Gouvernement
    const organeName = "Gouvernement"

    steps.push({
      ...loi,
      date: dateFromNullableDateOrIsoString(loi.proaccdat, undefined)!,
      index: -1,
      kind: SenatStepKind.EngagementProcedureAcceleree,
      lecass,
      lecture,
      organeCodeType,
      organeName,
      subOrganes: [null],
    })
    if (loi.proaccoppdat !== null) {
      steps.push({
        ...loi,
        date: dateFromNullableDateOrIsoString(loi.proaccoppdat, undefined)!,
        index: -1,
        kind: SenatStepKind.OppositionProcedureAcceleree,
        lecass,
        lecture,
        organeCodeType,
        organeName,
        subOrganes: [null], // TODO: This is not the governement that opposes to "procédure accélérée".
      })
    }
    if (loi.retproaccdat !== null) {
      steps.push({
        ...loi,
        date: dateFromNullableDateOrIsoString(loi.retproaccdat, undefined)!,
        index: -1,
        kind: SenatStepKind.RetraitProcedureAcceleree,
        lecass,
        lecture,
        organeCodeType,
        organeName,
        subOrganes: [null],
      })
    }
  }

  if (loi.saisine_date !== null) {
    // Saisine du Conseil constitutionnel
    const lecass: LecAssSimple = {
      ass: {
        codass: "C",
        libass: "Conseil constitutionnel",
      },
      lecassidt: "saisine_conseil_constitutionnel",
    }
    const lecture: LectureSimple = {
      lecasss: [lecass],
      leccom: "Conseil constitutionnel",
    }
    const organeCodeType = CodeTypeOrgane.Constitu
    const organeName = "Conseil constitutionnel"

    steps.push({
      ...loi,
      date: dateFromNullableDateOrIsoString(loi.saisine_date, undefined)!,
      index: -1,
      kind: SenatStepKind.SaisineConseilConstitutionnel,
      lecass,
      lecture,
      organeCodeType,
      organeName,
      subOrganes: [null], // TODO: a "saisine" is seldom done by Conseil contitutionnel.
    })
    if (loi.date_decision !== null) {
      steps.push({
        ...loi,
        date: dateFromNullableDateOrIsoString(loi.date_decision, undefined)!,
        index: -1,
        kind: SenatStepKind.DecisionConseilConstitutionnel,
        lecass,
        lecture,
        organeCodeType,
        organeName,
        subOrganes: [null],
      })
    }
  }

  if (loi.date_loi !== null) {
    // Promulgation de la loi
    const lecass: LecAssSimple = {
      ass: {
        codass: "G",
        libass: "Gouvernement",
      },
      lecassidt: "promulgation_loi",
    }
    const lecture: LectureSimple = {
      lecasss: [lecass],
      leccom: "Gouvernement",
    }
    const organeCodeType = CodeTypeOrgane.Gouvernement
    const organeName = "Gouvernement"

    steps.push({
      ...loi,
      date: dateFromNullableDateOrIsoString(loi.date_loi, undefined)!,
      index: -1,
      kind: SenatStepKind.PromulgationLoi,
      lecass,
      lecture,
      organeCodeType,
      organeName,
      subOrganes: [null],
    })
    if (loi.loidatjo !== null) {
      steps.push({
        ...loi,
        date: dateFromNullableDateOrIsoString(loi.loidatjo, undefined)!,
        index: -1,
        kind: SenatStepKind.PublicationLoi,
        lecass,
        lecture,
        organeCodeType,
        organeName,
        subOrganes: [null],
      })
    }
    if (loi.loidatjo2 !== null) {
      steps.push({
        ...loi,
        date: dateFromNullableDateOrIsoString(loi.loidatjo2, undefined)!,
        index: -1,
        kind: SenatStepKind.PublicationPremierCorrectifLoi,
        lecass,
        lecture,
        organeCodeType,
        organeName,
        subOrganes: [null],
      })
    }
    if (loi.loidatjo3 !== null) {
      steps.push({
        ...loi,
        date: dateFromNullableDateOrIsoString(loi.loidatjo3, undefined)!,
        index: -1,
        kind: SenatStepKind.PublicationSecondCorrectifLoi,
        lecass,
        lecture,
        organeCodeType,
        organeName,
        subOrganes: [null],
      })
    }
  }

  // Sort steps by date.
  // Before sort, add an index to each step to be sure to have a stable sort by date.
  steps.map((step, index) => {
    step.index = index
    return step
  })
  steps.sort((step1, step2) => {
    if (step1.date === step2.date) {
      return step1.kind === step2.kind
        ? stepNumber(step1) - stepNumber(step2)
        : stepPriorityByKind[step1.kind] - stepPriorityByKind[step2.kind]
    }
    if (step1.date === undefined || step2.date === undefined) {
      // When a step has no date, keep the original order.
      return step1.index - step2.index
    }
    // Ensure that dates without time are sorted after dates with time.
    const date1 = step1.date.toISOString().replace(/T00:00:00/, "T99:99:99")
    const date2 = step2.date.toISOString().replace(/T00:00:00/, "T99:99:99")
    return date1 < date2 ? -1 : date1 === date2 ? step1.index - step2.index : 1
  })

  return steps
}

function stepTitleFromLecture(lecture: Lecture): string | undefined {
  const typlec = lecture.typlec
  if (typlec === undefined) {
    return undefined
  }
  const titleByTypleccod: { [typleccod: string]: string } = {
    "1": "1ère lecture",
    "2": "2ème lecture", // TODO: To compare with Assemblée title
    "3": "3ème lecture", // TODO: To compare with Assemblée title
    "4": "Commission mixe paritaire", // TODO: To compare with Assemblée title
    "5": "Nouvelle lecture", // TODO: To compare with Assemblée title
    "6": "Lecture définitive", // TODO: To compare with Assemblée title
    "7": "4ème lecture", // TODO: To compare with Assemblée title
    "8": "Congrès", // TODO: To compare with Assemblée title
    "9": "Référendum", // TODO: To compare with Assemblée title
  }
  return titleByTypleccod[typlec.typleccod] || typlec.typleclib
}
