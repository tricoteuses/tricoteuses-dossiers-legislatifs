import assert from "assert"
import commandLineArgs from "command-line-args"
import { execSync } from "child_process"
import frontMatter from "front-matter"
import fs from "fs-extra"
import * as git from "isomorphic-git"
import jsonCycle from "json-cycle"
import jsYaml from "js-yaml"
import path from "path"

import config from "../config"
import {
  assembleeTextesStepsFromDossier,
  chronologyFromAssembleeSteps,
  chronologyFromSenatSteps,
  mergeAssembleeAndSenatChronologies,
  senatTextesStepsFromLoi,
} from "../chronologies"
import {
  DateBlock,
  Dossier,
  Loi,
  SenatStepKind,
  TexteStep,
} from "../types"

interface FrontMatterAttributes {
  id: number | string
  url: string
}

interface MarkdownData {
  attributes: FrontMatterAttributes
  body: string
  path: string
}

const { gitlab } = config
const optionsDefinitions = [
  {
    alias: "p",
    help: "push changes",
    name: "push",
    type: Boolean,
  },
  {
    alias: "s",
    help: "don't log anything",
    name: "silent",
    type: Boolean,
  },
  {
    alias: "v",
    help: "verbose logs",
    name: "verbose",
    type: Boolean,
  },
  {
    defaultOption: true,
    help: 'directory of a "dossier législatif"',
    name: "dossierDir",
    type: String,
  },
]
const options = commandLineArgs(optionsDefinitions)

async function commitMarkdownData(
  dateLocalIsoString: string | undefined,
  message: string,
  data: MarkdownData,
): Promise<boolean> {
  const dossierDir = options.dossierDir
  fs.writeFileSync(
    path.join(dossierDir, "texte_de_loi.md"),
    `---\n${jsYaml.safeDump(data.attributes, { sortKeys: true })}---\n\n${data.body}`,
  )
  const statusMatrix = await git.statusMatrix({
    dir: dossierDir,
  })
  let commitNeeded = false
  for (const [
    filepath,
    headStatus,
    workTreeStatus,
    // stateStatus,
  ] of statusMatrix) {
    if (workTreeStatus === 0) {
      if (headStatus === 1) {
        // File deleted
        await git.remove({
          dir: dossierDir,
          filepath,
        })
        commitNeeded = true
      }
    } else if (workTreeStatus === 2) {
      // File added or modified
      await git.add({
        dir: dossierDir,
        filepath,
      })
      commitNeeded = true
    }
  }
  if (commitNeeded) {
    await git.commit({
      author: {
        date:
          dateLocalIsoString === undefined ? new Date() : new Date(dateLocalIsoString),
      },
      dir: dossierDir,
      message,
    })
  }
  return commitNeeded
}

async function main() {
  const dossierDir = options.dossierDir
  assert(fs.statSync(dossierDir).isDirectory(), `${dossierDir} is not a directory`)

  if (!options.silent) {
    console.log(`Generating master branch for ${dossierDir}…`)
  }

  const branches = await git.listBranches({ dir: dossierDir })
  const currentBranch = await git.currentBranch({ dir: dossierDir })
  if (currentBranch !== "edition") {
    assert(branches.includes("edition"))
    await git.checkout({ dir: dossierDir, ref: "edition" })
  }

  let assembleeChronology: DateBlock[] | null = null
  const assembleeMarkdownDataById: { [id: string]: MarkdownData } = {}
  const assembleeDir = path.join(dossierDir, "assemblee")
  let dossier: Dossier | null = null
  if (fs.statSync(assembleeDir).isDirectory()) {
    if (!options.silent) {
      console.log(`Loading Assemblée Markdown "textes" at ${assembleeDir}…`)
    }

    const dossierFilePath = path.join(assembleeDir, "dossier.json")
    const dossierJson = fs.readFileSync(dossierFilePath, {
      encoding: "utf8",
    })
    dossier = jsonCycle.parse(dossierJson)
    const steps = assembleeTextesStepsFromDossier(dossier!)
    assembleeChronology = chronologyFromAssembleeSteps(steps)

    for (const filename of fs.readdirSync(assembleeDir)) {
      if (filename[0] === "." || !filename.endsWith(".md")) {
        continue
      }
      const markdownFilePath = path.join(assembleeDir, filename)
      const data = fs.readFileSync(markdownFilePath, {
        encoding: "utf8",
      })
      const { attributes, body } = frontMatter(data)
      assembleeMarkdownDataById[attributes.id] = {
        attributes,
        body,
        path: markdownFilePath,
      }
    }
  }

  let loi: Loi | null = null
  let senatChronology: DateBlock[] | null = null
  const senatMarkdownDataById: { [filename: string]: MarkdownData } = {}
  const senatDir = path.join(dossierDir, "senat")
  if (fs.statSync(senatDir).isDirectory()) {
    if (!options.silent) {
      console.log(`Loading Sénat Markdown "textes" at ${senatDir}…`)
    }

    const loiFilePath = path.join(senatDir, "dossier.json")
    const loiJson = fs.readFileSync(loiFilePath, {
      encoding: "utf8",
    })
    loi = jsonCycle.parse(loiJson)
    const steps = senatTextesStepsFromLoi(loi!)
    senatChronology = chronologyFromSenatSteps(steps)

    for (const filename of fs.readdirSync(senatDir)) {
      if (filename[0] === "." || !filename.endsWith(".md")) {
        continue
      }
      const markdownFilePath = path.join(senatDir, filename)
      const data = fs.readFileSync(markdownFilePath, {
        encoding: "utf8",
      })
      const { attributes, body } = frontMatter(data)
      senatMarkdownDataById[attributes.id] = {
        attributes,
        body,
        path: markdownFilePath,
      }
    }
  }

  // Delete "master" branch when it exists.
  if (branches.includes("master")) {
    execSync("git branch -D master", {
      cwd: dossierDir,
      env: process.env,
      encoding: "utf-8",
      // stdio: ["ignore", "ignore", "pipe"],
    })
  }

  // Create an orphan "master" branch.
  // This is currently not possible with "isomorphic-git".
  execSync("git checkout --orphan master", {
    cwd: dossierDir,
    env: process.env,
    encoding: "utf-8",
    stdio: ["ignore", "ignore", "pipe"],
  })
  execSync("git rm -rf .", {
    cwd: dossierDir,
    env: process.env,
    encoding: "utf-8",
    // stdio: ["ignore", "ignore", "pipe"],
  })

  const chronology = mergeAssembleeAndSenatChronologies(
    assembleeChronology,
    senatChronology,
  )

  let pushNeeded = false
  for (const dateBlock of chronology) {
    // dateBlock.dateLocalIsoString === undefined

    for (const organeBlockKey of dateBlock.keys) {
      const organeBlock = dateBlock.organeBlockByKey[organeBlockKey]

      for (const subOrganeBlockKey of organeBlock.keys) {
        const subOrganeBlock = organeBlock.subOrganeBlockByKey[subOrganeBlockKey]

        if (subOrganeBlock.assembleeSteps !== undefined) {
          for (const step of subOrganeBlock.assembleeSteps!) {
            const messageFragments = []
            if (organeBlock.originOrganeName !== undefined) {
              messageFragments.push(organeBlock.originOrganeName, " → ")
            }
            messageFragments.push(organeBlock.organeName)
            if (organeBlock.title !== undefined) {
              messageFragments.push(", ", organeBlock.title)
            }
            if (subOrganeBlock.title) {
              messageFragments.push(", ", subOrganeBlock.title)
            }

            const texteAssocie = step.leafActe.texteAssocie
            if (texteAssocie !== undefined) {
              const markdownData = assembleeMarkdownDataById[texteAssocie.uid]
              if (markdownData === undefined) {
                console.log("Missing Assemblée file for:", texteAssocie.uid)
              } else if (
                await commitMarkdownData(
                  dateBlock.dateLocalIsoString,
                  messageFragments.join(""),
                  markdownData,
                )
              ) {
                pushNeeded = true
              }
            }

            const texteAdopte = step.leafActe.texteAdopte
            if (texteAdopte !== undefined) {
              const markdownData = assembleeMarkdownDataById[texteAdopte.uid]
              if (markdownData === undefined) {
                console.log("Missing Assemblée file for:", texteAdopte.uid)
              } else if (
                await commitMarkdownData(
                  dateBlock.dateLocalIsoString,
                  messageFragments.join(""),
                  markdownData,
                )
              ) {
                pushNeeded = true
              }
            }
          }
        }

        if (subOrganeBlock.senatSteps !== undefined) {
          for (const step of subOrganeBlock.senatSteps!) {
            const messageFragments = []
            if (organeBlock.originOrganeName !== undefined) {
              messageFragments.push(organeBlock.originOrganeName, " → ")
            }
            messageFragments.push(organeBlock.organeName)
            if (organeBlock.title !== undefined) {
              messageFragments.push(", ", organeBlock.title)
            }
            if (subOrganeBlock.title) {
              messageFragments.push(", ", subOrganeBlock.title)
            }

            assert.strictEqual(step.kind, SenatStepKind.Texte)
            const texteStep = step as TexteStep
            const markdownData = senatMarkdownDataById[texteStep.texcod]
            if (markdownData === undefined) {
              console.log("Missing Sénat file for:", texteStep.texcod)
            } else if (
              await commitMarkdownData(
                dateBlock.dateLocalIsoString,
                messageFragments.join(""),
                markdownData,
              )
            ) {
              pushNeeded = true
            }
          }
        }
      }
    }
  }

  if (pushNeeded && options.push) {
    const pushResponse = await git.push({
      dir: dossierDir,
      force: true,
      oauth2format: "gitlab",
      remote: "origin",
      token: gitlab.accessToken,
    })
    assert.strictEqual(pushResponse.errors, undefined)
  }
}

git.plugins.set("fs", fs)

main().catch(error => {
  console.log(error)
  process.exit(1)
})
