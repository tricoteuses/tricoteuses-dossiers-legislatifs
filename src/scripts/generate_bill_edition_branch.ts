import assert from "assert"
import { execSync } from "child_process"
import commandLineArgs from "command-line-args"
import fs from "fs-extra"
import * as git from "isomorphic-git"

import config from "../config"

const { gitlab } = config
const optionsDefinitions = [
  {
    alias: "p",
    help: "push changes",
    name: "push",
    type: Boolean,
  },
  {
    alias: "s",
    help: "don't log anything",
    name: "silent",
    type: Boolean,
  },
  {
    alias: "v",
    help: "verbose logs",
    name: "verbose",
    type: Boolean,
  },
  {
    defaultOption: true,
    help: 'directory of a "dossier législatif"',
    name: "dossierDir",
    type: String,
  },
]
const options = commandLineArgs(optionsDefinitions)

async function main() {
  const dossierDir = options.dossierDir
  assert(fs.statSync(dossierDir).isDirectory(), `${dossierDir} is not a directory`)

  const currentBranch = await git.currentBranch({ dir: dossierDir })
  let mergeNeeded = false
  let pushNeeded = false
  if (currentBranch === "edition") {
    mergeNeeded = true
  } else {
    const branches = await git.listBranches({ dir: dossierDir })
    if (branches.includes("edition")) {
      await git.checkout({ dir: dossierDir, ref: "edition" })
      mergeNeeded = true
    } else {
      // Create branch.
      await git.branch({ checkout: true, dir: dossierDir, ref: "edition" })
      pushNeeded = true
    }
  }
  if (mergeNeeded) {
    // First, simulate merge to test whether it will succeed.
    const dryMergeReport = await git.merge({
      dir: dossierDir,
      dryRun: true,
      theirs: "textes_markdown",
    })
    console.log("Dry merge:", dryMergeReport)
    const mergeReport = await git.merge({
      dir: dossierDir,
      theirs: "textes_markdown",
    })
    console.log("Real merge:", mergeReport)
    // Remove files in staged aread. This is needed because of a bug/feature
    // of isomorphic-git.
    execSync("git reset --hard edition", {
      cwd: dossierDir,
      env: process.env,
      encoding: "utf-8",
      // stdio: ["ignore", "ignore", "pipe"],
    })
    if (!mergeReport.alreadyMerged) {
      pushNeeded = true
    }
  }
  if (options.push && pushNeeded) {
    const pushResponse = await git.push({
      dir: dossierDir,
      oauth2format: "gitlab",
      remote: "origin",
      token: gitlab.accessToken,
    })
    assert.strictEqual(pushResponse.errors, undefined)
  }
}

git.plugins.set("fs", fs)

main().catch(error => {
  console.log(error)
  process.exit(1)
})
