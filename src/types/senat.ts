import {
  Aud,
  DateSeance,
  LecAss,
  LecAssRap,
  Lecture,
  Loi as LoiOriginal,
  Org,
  Texte,
} from "@tricoteuses/senat"

import { StepCodeTypeOrgane, Tricot } from "./shared"

interface AmendementsCommissionStep extends LecAss, StepShared {
  kind: SenatStepKind.AmendementsCommission
}

interface AmendementsSeanceStep extends LecAss, StepShared {
  kind: SenatStepKind.AmendementsSeance
}

interface AssSimple {
  codass: string
  libass: string
}

interface AudStep extends Aud, StepShared {
  kind: SenatStepKind.Aud
}

interface DateSeanceStep extends DateSeance, StepShared {
  kind: SenatStepKind.DateSeance
}

interface DecisionConseilConstitutionnelStep extends Loi, StepShared {
  kind: SenatStepKind.DecisionConseilConstitutionnel
}

interface EngagementProcedureAccelereeStep extends Loi, StepShared {
  kind: SenatStepKind.EngagementProcedureAcceleree
}

interface LecAssRapStep extends LecAssRap, StepShared {
  kind: SenatStepKind.LecAssRap
}

export interface LecAssSimple {
  ass: AssSimple
  lecassidt: string
}

export interface LectureSimple {
  lecasss: LecAssSimple[]
  leccom: string
}

export interface Loi extends LoiOriginal {
  tricot?: Tricot
}

interface OppositionProcedureAccelereeStep extends Loi, StepShared {
  kind: SenatStepKind.OppositionProcedureAcceleree
}

interface PromulgationLoiStep extends Loi, StepShared {
  kind: SenatStepKind.PromulgationLoi
}

interface PublicationLoiStep extends Loi, StepShared {
  kind: SenatStepKind.PublicationLoi
}

interface PublicationPremierCorrectifLoiStep extends Loi, StepShared {
  kind: SenatStepKind.PublicationPremierCorrectifLoi
}

interface PublicationSecondCorrectifLoiStep extends Loi, StepShared {
  kind: SenatStepKind.PublicationSecondCorrectifLoi
}

interface RetraitProcedureAccelereeStep extends Loi, StepShared {
  kind: SenatStepKind.RetraitProcedureAcceleree
}

interface SaisineConseilConstitutionnelStep extends Loi, StepShared {
  kind: SenatStepKind.SaisineConseilConstitutionnel
}

export type SenatStep =
  | AmendementsCommissionStep
  | AmendementsSeanceStep
  | AudStep
  | DateSeanceStep
  | DecisionConseilConstitutionnelStep
  | EngagementProcedureAccelereeStep
  | LecAssRapStep
  | OppositionProcedureAccelereeStep
  | PromulgationLoiStep
  | PublicationLoiStep
  | PublicationPremierCorrectifLoiStep
  | PublicationSecondCorrectifLoiStep
  | RetraitProcedureAccelereeStep
  | SaisineConseilConstitutionnelStep
  | TexteStep

export enum SenatStepKind {
  AmendementsCommission = "AmendementsCommission",
  AmendementsSeance = "AmendementsSeance",
  Aud = "Aud",
  DateSeance = "DateSeance",
  DecisionConseilConstitutionnel = "DecisionConseilConstitutionnel",
  EngagementProcedureAcceleree = "EngagementProcedureAcceleree",
  LecAssRap = "LecAssRap",
  OppositionProcedureAcceleree = "OppositionProcedureAcceleree",
  PromulgationLoi = "PromulgationLoi",
  PublicationLoi = "PublicationLoi",
  PublicationPremierCorrectifLoi = "PublicationPremierCorrectifLoi",
  PublicationSecondCorrectifLoi = "PublicationSecondCorrectifLoi",
  RetraitProcedureAcceleree = "RetraitProcedureAcceleree",
  SaisineConseilConstitutionnel = "SaisineConseilConstitutionnel",
  Texte = "Texte"
}

interface StepShared {
  date?: Date
  index: number
  lecass: LecAss | LecAssSimple
  lecture: Lecture | LectureSimple
  organeCodeType: StepCodeTypeOrgane
  organeName: string
  originOrganeCodeType?: StepCodeTypeOrgane
  originOrganeName?: string
  subOrganes: (Org | null)[]
  title?: string
}

export interface TexteStep extends StepShared, Texte {
  kind: SenatStepKind.Texte
}
