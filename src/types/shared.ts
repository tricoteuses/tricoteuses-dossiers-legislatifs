import {
  CodeTypeOrgane,
} from "@tricoteuses/assemblee"

export type StepCodeTypeOrgane = CodeTypeOrgane | StepOnlyCodeTypeOrgane

export enum StepOnlyCodeTypeOrgane {
  Autre = "AUTRE",
  Ce = "CE", // Conseil d'État
  Internet = "INTERNET",
}

interface TexteMetadata {
  filename: string
  id: number | string
  /// True when texte is a draft of "texte de loi" and not another associated document.
  isBillDraft: boolean
  title: string
  url: string
}

export interface Tricot {
  textesMetadata: TexteMetadata[]
}
