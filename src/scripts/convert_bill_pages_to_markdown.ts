import assert from "assert"
import { execSync } from "child_process"
import commandLineArgs from "command-line-args"
import fs from "fs-extra"
import * as git from "isomorphic-git"
import jsYaml from "js-yaml"
import { JSDOM } from "jsdom"
import jsonCycle from "json-cycle"
import path from "path"
import TurndownService from "turndown"
import windows1252 from "windows-1252"

import config from "../config"
import { restructureTexteMarkdown } from "../markdown"
import { Dossier, Loi } from "../types"

const { gitlab } = config
const optionsDefinitions = [
  {
    alias: "c",
    help: "commit files",
    name: "commit",
    type: Boolean,
  },
  {
    alias: "p",
    help: "push changes",
    name: "push",
    type: Boolean,
  },
  {
    alias: "s",
    help: "don't log anything",
    name: "silent",
    type: Boolean,
  },
  {
    alias: "v",
    help: "verbose logs",
    name: "verbose",
    type: Boolean,
  },
  {
    defaultOption: true,
    help:
      'directory of a "dossier législatif" of path of a single HTML page in a "dossier législatif"',
    name: "dossierDirOrPageFilePath",
    type: String,
  },
]
const options = commandLineArgs(optionsDefinitions)

function extractAssembleeTexteFromPage(
  _filename: string,
  assembleeUrl: string,
  page: string,
): string | null {
  // Repair HTML.
  const pageSegments = page.split("</style>")
  if (pageSegments.length !== 2) {
    if (!options.silent) {
      console.log("  HTML page is not a page embedding raw HTML with <style>…")
    }
    return null
  }
  let html = pageSegments[1].trim()
  if (!html.startsWith("<div")) {
    if (!options.silent) {
      console.log("  Embedded HTML page doesn't start with a <div>…")
    }
    return null
  }

  if (!html.endsWith("</div>")) {
    if (!options.silent) {
      console.log("  Embedded HTML page doesn't end with a </div>…")
    }
    return null
  }
  html = `<html${html.substring(4, html.length - 6)}</html>`

  // Extract subdivisions from HTML.
  const { window } = new JSDOM(html)
  const { document } = window
  assert.strictEqual(document.children.length, 1)
  const htmlElement = document.children[0]
  assert.strictEqual(htmlElement.children.length, 2)
  const bodyElement = htmlElement.children[1]
  if (assembleeUrl === "http://www.assemblee-nationale.fr/15/textes/0675.asp") {
    // Repair document.
    assert.strictEqual(bodyElement.children.length, 1)
    const divElement = bodyElement.children[0]
    assert.strictEqual(divElement.tagName, "DIV")
    const newDivElement = bodyElement.insertBefore(
      document.createElement("div"),
      divElement,
    )
    bodyElement.insertBefore(document.createElement("br"), divElement)
    for (const divChild of [...divElement.children]) {
      if (divChild.getAttribute("class") === "a9ArticleNum") {
        break
      }
      newDivElement.appendChild(divChild)
    }
  }
  if (bodyElement.children.length < 3) {
    // "Texte" is missing from page.
    // Occurs in http://www.assemblee-nationale.fr/15/textes/0326.asp.
    window.close() // Free memory.
    return null
  }
  assert.strictEqual(bodyElement.children[0].tagName, "DIV")
  // First child is a DIV describing the document (Assemblée's header). Skip it for now.
  let bodyChild = bodyElement.children[1]
  assert.strictEqual(bodyChild.tagName, "BR")
  if (bodyChild.nextElementSibling === null) {
    // "Texte" is missing from page.
    window.close() // Free memory.
    return null
  }
  const texteElement = bodyChild.nextElementSibling
  const texteHtml = texteElement.outerHTML
  window.close() // Free memory.
  return texteHtml
}

function extractSenatTexteFromPage(
  filename: string,
  _senatUrl: string,
  page: string,
): string | null {
  assert(filename.endsWith(".html"), filename)

  const { window } = new JSDOM(page)
  const { document } = window
  const alineasRootDivElement = document.getElementById(
    "formCorrection:tableauComparatif",
  )
  assert.notStrictEqual(alineasRootDivElement, null)
  assert.strictEqual(alineasRootDivElement!.tagName, "DIV")
  for (const alineasRootDivChild of alineasRootDivElement!.children) {
    if (alineasRootDivChild.tagName !== "DIV") {
      alineasRootDivElement!.removeChild(alineasRootDivChild)
      continue
    }
    const id = alineasRootDivChild.getAttribute("id")
    assert.notStrictEqual(id, null)
    assert.notStrictEqual(id!.match(/^formCorrection:panel:\d+:table$/), null)
    for (const tbodyElement of alineasRootDivChild!.getElementsByTagName("tbody")) {
      for (const trElement of tbodyElement.children) {
        // Ensure that there is a pastille in TR.
        const pastilleTdElement = trElement.children[0]
        const pastilleDivElements = [...pastilleTdElement.children]
        if (pastilleDivElements.length !== 0) {
          assert.strictEqual(pastilleDivElements.length, 1)
          const pastilleDivElement = pastilleDivElements[0]
          assert.strictEqual(pastilleDivElement.tagName, "DIV")
          const pastillePElements = [...pastilleDivElement.children]
          assert.strictEqual(pastillePElements.length, 1)
          const pastillePElement = pastillePElements[0]
          assert.strictEqual(pastillePElement.tagName, "P")
        }
        // Delete pastille.
        trElement.removeChild(pastilleTdElement)
      }
    }
  }

  const texteHtml = alineasRootDivElement!.outerHTML
  window.close() // Free memory.
  return texteHtml
}

async function main() {
  let dossierDir
  let pageToConvertFilePath
  if (
    fs.existsSync(options.dossierDirOrPageFilePath) &&
    fs.statSync(options.dossierDirOrPageFilePath).isDirectory()
  ) {
    dossierDir = options.dossierDirOrPageFilePath
    pageToConvertFilePath = null
  } else {
    pageToConvertFilePath = options.dossierDirOrPageFilePath
    dossierDir = path.dirname(path.dirname(pageToConvertFilePath))
  }
  assert(fs.statSync(dossierDir).isDirectory(), `${dossierDir} is not a directory`)

  const turndownService = new TurndownService({
    blankReplacement: function(content) {
      return content
    },
  })

  const currentBranch = await git.currentBranch({ dir: dossierDir })
  if (currentBranch !== "pages_originales") {
    const branches = await git.listBranches({ dir: dossierDir })
    assert(branches.includes("pages_originales"))
    await git.checkout({ dir: dossierDir, ref: "pages_originales" })
  }

  const assembleeDir = path.join(dossierDir, "assemblee")
  const assembleeTexteMardkownByFilename: { [filename: string]: string } = {}
  const assembleeTexteRawByFilename: { [filename: string]: string } = {}
  let dossier: Dossier | null = null
  if (fs.statSync(assembleeDir).isDirectory()) {
    if (!options.silent) {
      console.log(`Converting Assemblée "textes" at ${assembleeDir} to Markdown…`)
    }

    const dossierFilePath = path.join(assembleeDir, "dossier.json")
    const dossierJson: string = fs.readFileSync(dossierFilePath, {
      encoding: "utf8",
    })
    dossier = jsonCycle.parse(dossierJson)
    for (const texteMetadata of dossier!.tricot!.textesMetadata) {
      if (!texteMetadata.isBillDraft) {
        continue
      }
      const pageFilePath = path.join(assembleeDir, texteMetadata.filename)
      if (pageToConvertFilePath !== null && pageFilePath !== pageToConvertFilePath) {
        continue
      }
      if (!options.silent) {
        console.log(
          `  Converting Assemblée "${texteMetadata.filename}" <${texteMetadata.url}> to Markdown…`,
        )
      }
      let texteMarkdown = null
      let texteRaw = null
      if (texteMetadata.filename.endsWith(".pdf")) {
        texteRaw = execSync(`pdftotext -nopgbrk ${texteMetadata.filename} -`, {
          cwd: assembleeDir,
          env: process.env,
          encoding: "utf-8",
          // stdio: ["ignore", "pipe", "pipe"],
        })
        texteMarkdown = texteRaw!
          // Remove page numbers.
          .replace(/^– \d+ –$/gm, "")
          // Remove Unicode private use area used for "pastilles".
          .replace(/[\u{F000}-\u{FFFD}]/gu, "")
          // Replace multiple spaces with a single space.
          .replace(/ {2,}/g, " ")
        texteMarkdown = restructureTexteMarkdown(texteMarkdown, [texteMetadata.title])
        texteMarkdown = texteMarkdown
          // Replace sequences of at least 2 \n with exactly 2 \n
          // and merge lines separated by a single \n.
          .replace(/\n{2,}/g, "\u{F000}")
          .replace(/\n/g, " ")
          .replace(/\u{F000}/gu, "\n\n")
      } else {
        // HTML page
        const page = fs.readFileSync(pageFilePath, {
          encoding: "utf8",
        })
        let texteHtml = extractAssembleeTexteFromPage(
          texteMetadata.filename,
          texteMetadata.url,
          page,
        )
        if (texteHtml === null) {
          texteHtml = windows1252.decode(
            fs.readFileSync(pageFilePath).toString("binary"),
            {
              mode: "fatal",
            },
          )
        }
        texteRaw = turndownService.turndown(texteHtml!)
        texteMarkdown = restructureTexteMarkdown(texteRaw, [texteMetadata.title])
      }

      assembleeTexteRawByFilename[texteMetadata.filename] = texteRaw
      assembleeTexteMardkownByFilename[texteMetadata.filename] = texteMarkdown
    }
  }

  let loi: Loi | null = null
  const senatDir = path.join(dossierDir, "senat")
  const senatTexteMardkownByFilename: { [filename: string]: string } = {}
  const senatTexteRawByFilename: { [filename: string]: string } = {}
  if (fs.statSync(senatDir).isDirectory()) {
    if (!options.silent) {
      console.log(`Converting Sénat "textes" at ${senatDir} to Markdown…`)
    }
    const loiFilePath = path.join(senatDir, "dossier.json")
    const loiJson: string = fs.readFileSync(loiFilePath, {
      encoding: "utf8",
    })
    loi = jsonCycle.parse(loiJson)
    for (const texteMetadata of loi!.tricot!.textesMetadata) {
      if (!texteMetadata.isBillDraft) {
        continue
      }
      const pageFilePath = path.join(senatDir, texteMetadata.filename)
      if (pageToConvertFilePath !== null && pageFilePath !== pageToConvertFilePath) {
        continue
      }
      if (!options.silent) {
        console.log(
          `  Converting Sénat "${texteMetadata.filename}" <${texteMetadata.url}> to Markdown…`,
        )
      }
      const page = windows1252.decode(fs.readFileSync(pageFilePath).toString("binary"), {
        mode: "fatal",
      })
      const texteHtml = extractSenatTexteFromPage(
        texteMetadata.filename,
        texteMetadata.url,
        page,
      )
      if (texteHtml === null) {
        continue
      }

      const texteRaw = turndownService.turndown(texteHtml)
      senatTexteRawByFilename[texteMetadata.filename] = texteRaw

      const texteMarkdown = restructureTexteMarkdown(texteRaw, [texteMetadata.title])
      senatTexteMardkownByFilename[texteMetadata.filename] = texteMarkdown
    }
  }

  const branches = await git.listBranches({ dir: dossierDir })
  if (branches.includes("textes_markdown")) {
    await git.checkout({ dir: dossierDir, ref: "textes_markdown" })
  } else {
    // Create an orphan branch.
    // This is currently not possible with "isomorphic-git".
    execSync("git checkout --orphan textes_markdown", {
      cwd: dossierDir,
      env: process.env,
      encoding: "utf-8",
      stdio: ["ignore", "ignore", "pipe"],
    })
    execSync("git rm -rf .", {
      cwd: dossierDir,
      env: process.env,
      encoding: "utf-8",
      // stdio: ["ignore", "ignore", "pipe"],
    })
  }

  // Erase repository content before regenerating it, but only if it will be fully regenerated.
  if (pageToConvertFilePath === null) {
    for (const nodeName of fs.readdirSync(dossierDir)) {
      if (nodeName[0] === ".") {
        continue
      }
      fs.removeSync(path.join(dossierDir, nodeName))
    }
  }

  if (Object.keys(assembleeTexteMardkownByFilename).length > 0) {
    fs.ensureDirSync(assembleeDir)
    for (const texteMetadata of dossier!.tricot!.textesMetadata) {
      const pageFilename = texteMetadata.filename

      const texteRaw = assembleeTexteRawByFilename[pageFilename]
      if (texteRaw !== undefined) {
        const texteFilename = path.parse(pageFilename).name + "_brut.md"
        fs.writeFileSync(
          path.join(assembleeDir, texteFilename),
          `---\n${jsYaml.safeDump(
            { id: texteMetadata.id, url: texteMetadata.url },
            { sortKeys: true },
          )}---\n\n${texteRaw}`,
        )
      }

      const texteMarkdown = assembleeTexteMardkownByFilename[pageFilename]
      if (texteMarkdown !== undefined) {
        const texteFilename = path.parse(pageFilename).name + ".md"
        fs.writeFileSync(
          path.join(assembleeDir, texteFilename),
          `---\n${jsYaml.safeDump(
            { id: texteMetadata.id, url: texteMetadata.url },
            { sortKeys: true },
          )}---\n\n${texteMarkdown}`,
        )
      }
    }
    fs.writeFileSync(
      path.join(assembleeDir, "dossier.json"),
      jsonCycle.stringify(dossier, null, 2),
    )
  }
  if (Object.keys(senatTexteMardkownByFilename).length > 0) {
    fs.ensureDirSync(senatDir)
    for (const texteMetadata of loi!.tricot!.textesMetadata) {
      const pageFilename = texteMetadata.filename

      const texteRaw = senatTexteRawByFilename[pageFilename]
      if (texteRaw !== undefined) {
        const texteFilename = path.parse(pageFilename).name + "_brut.md"
        fs.writeFileSync(
          path.join(senatDir, texteFilename),
          `---\n${jsYaml.safeDump(
            { id: texteMetadata.id, url: texteMetadata.url },
            { sortKeys: true },
          )}---\n\n${texteRaw}`,
        )
      }

      const texteMarkdown = senatTexteMardkownByFilename[pageFilename]
      if (texteMarkdown !== undefined) {
        const texteFilename = path.parse(pageFilename).name + ".md"
        fs.writeFileSync(
          path.join(senatDir, texteFilename),
          `---\n${jsYaml.safeDump(
            { id: texteMetadata.id, url: texteMetadata.url },
            { sortKeys: true },
          )}---\n\n${texteMarkdown}`,
        )
      }
    }
    fs.writeFileSync(
      path.join(senatDir, "dossier.json"),
      jsonCycle.stringify(loi, null, 2),
    )
  }

  if (options.commit) {
    let commitNeeded = false
    const statusMatrix = await git.statusMatrix({
      dir: dossierDir,
    })
    for (const [
      filepath,
      headStatus,
      workTreeStatus,
      // stateStatus,
    ] of statusMatrix) {
      if (workTreeStatus === 0) {
        if (headStatus === 1) {
          // File deleted
          await git.remove({
            dir: dossierDir,
            filepath,
          })
          commitNeeded = true
        }
      } else if (workTreeStatus === 2) {
        // File added or modified
        await git.add({
          dir: dossierDir,
          filepath,
        })
        commitNeeded = true
      }
    }
    if (commitNeeded) {
      await git.commit({
        dir: dossierDir,
        message: "Conversion en Markdown",
      })
      if (options.push) {
        const pushResponse = await git.push({
          dir: dossierDir,
          oauth2format: "gitlab",
          remote: "origin",
          token: gitlab.accessToken,
        })
        assert.strictEqual(pushResponse.errors, undefined)
      }
    }
  }
}

git.plugins.set("fs", fs)

main().catch(error => {
  console.log(error)
  process.exit(1)
})
