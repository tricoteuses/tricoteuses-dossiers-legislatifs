# Tricoteuses-Dossiers-Législatifs

## _Generate Git repositories for the bills of the French Parliament_

This project has migrated to https://git.en-root.org/tricoteuses/tricoteuses-dossiers-legislatifs.

## Installation

```bash
git clone https://git.en-root.org/tricoteuses/tricoteuses-dossiers-legislatifs
cd tricoteuses-dossiers-legislatifs/
```

Create a `.env` file to set PostgreSQL database informations and other configuration variables (you can use `example.env` as a template). Then

```bash
npm install
```

## Usage

### Bills retrieval

Retrieve bills source HTML pages; store them into Git repositories;  push them to (existing or new) GitLab projects.

```bash
npx babel-node --extensions ".ts" -- src/scripts/fetch_original_pages.ts --dossier energie_climat --commit --push ../dossiers-legislatifs
```

### Bills to Markdown conversion

Convert HTML pages of a bill to Markdown:

```bash
npx babel-node --extensions ".ts" -- src/scripts/convert_bill_pages_to_markdown.ts --commit --push ../dossiers-legislatifs/R5L15/energie_climat/
```

Merge restructured Markdown into "edition" branch (aka the branch edited by users):

```bash
npx babel-node --extensions ".ts" -- src/scripts/generate_bill_edition_branch.ts --push ../dossiers-legislatifs/R5L15/energie_climat
```

Retrieve versions of "texte de loi" in "edition" branch and commit them chronologically in "master" branch:

```bash
npx babel-node --extensions ".ts" -- src/scripts/generate_bill_master_branch.ts --push ../dossiers-legislatifs/R5L15/energie_climat
```
