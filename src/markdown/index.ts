export {
  colorFromOrganeCodeType,
  colorFromStatus,
  emojiFromOrganeCodeType,
  emojiFromStatus,
  markdownLinesFromAssembleeStep,
} from "./dossiers/assemblee"
export { markdownLinesFromChronology } from "./dossiers/merged"
export {
  colorFromEtaloilib,
  emojiFromEtaloilib,
  markdownLinesFromSenatStep,
} from "./dossiers/senat"
export { coloredSpan, emojizedText } from "./shared"
export { restructureTexteMarkdown } from "./textes"
