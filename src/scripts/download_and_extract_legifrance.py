
#!/usr/bin/env python
# coding: utf-8

# In[194]:


import os
import sys
from pprint import pprint
from pathlib import Path
import subprocess
#import pandas as pd
#import math
import json
#import xml.dom.minidom
print("yes")


# In[191]:


main_url = "https://www.legifrance.gouv.fr/affichLoiPreparation.do;j?legislature=15&typeLoi=proj"
main_filename = "main.html"
path = "../../../"
to_extract_dirname = "raw_html/"
extracted_dirname = "extracted/"
urls_dossiers = "urls_dossiers_legifrance"
legislatures = {"15":['id=\"an2020\"', 'id=\"an2019\"', 'id=\"an2018\"', 'id=\"an2017\"'], "14":["2016", "2015", "2014", "2013"]}


# In[3]:


pwd = get_ipython().system("pwd")
pwd


# In[4]:


def curl_file(url, path_to_filename) :
    print(url)
    print(path_to_filename)
    chemin = path_to_filename
    try :
        with open(chemin, "w") as file:
            subprocess.run(["curl", url], stdout=file)
        return True
    except :
        print("curl problem, url problem, network problem")
        return False


# In[8]:


path_main = os.path.join(path, to_extract_dirname, main_filename)
print(path_main)
curl_file(main_url, path_main)


# In[14]:


def read_file (path):
    with open(path, encoding='utf-8', mode='r') as file:
        lines=""
        line = file.readline()
        while(line):
            line = file.readline()
            #print(line)
            lines+=line
    file.close()
    return lines


# In[10]:


def write_file(path, content_liste) :
    file = open(path, encoding='utf-8', mode='w')
    for line in content_liste:
        file.write(line)
    print("saved ", path)
    file.close()


# In[160]:


def clean_legifrance_url(url, prefix, const_el):
    assert(const_el in url)
    url = url.replace("&amp;", "&")
    chaine = const_el + "?"+url.split('?')[1].strip('\n').strip(" ").strip("href=").strip('"')
    return prefix + chaine


# In[161]:


prefix = '''https://www.legifrance.gouv.fr/'''
const_el = '''affichLoiPreparation.do;j'''
url = '''href="affichLoiPreparation.do;jsessionid=5C27E43B62588A2CB81CB6ABC59EE752.tplgfr23s_1?idDocument=JORFDOLE000039143245&amp;type=general&amp;typeLoi=proj&amp;legislature=15"'''
clean_legifrance_url(url, prefix, const_el)


# In[169]:


def clean_libelle(texte):
    print(texte)
    texte = texte.replace('\n',' ').replace('   ', ' ').replace('  ', ' ')
    print(texte)
    nom = texte.split('(')[0].strip(" ")
    id_legifr = texte.split('(')[1].strip(" ").strip(")")
    return nom, id_legifr


# In[170]:


texte = 'Projet de loi ratifiant l’ordonnance n° 2019-610 du 19 juin\n 2019 portant harmonisation de la terminologie du droit de \n l’armement dans le code de la défense et le code de la \n sécurité intérieure et portant diverses dispositions de \n coordination (ARMD1921465L)'
clean_libelle(texte)


# In[11]:


main_str = read_file(path_main)
pprint(main_str)


# In[172]:


def extract_DL_urls(main_url, main_filename):
    path_main = os.path.join(path, to_extract_dirname, main_filename)
    print(path_main)
    url_DL_liste = []
    if curl_file(main_url, path_main) :
        main_str = read_file(path_main)
        main_str.replace("&amp;","&")
        temp = main_str        .split('<!-- On insère le corps de la page -->')[1]        .strip('\n')        .strip(' ')        .split('<!-- On insère le pied de page -->')[0]        .strip('\n')        .strip(' ')
        #print("TEMP\n", temp)
        if len(temp.split('id=\"an2020\"')) > 1 :
            a_2020 = temp.split('id=\"an2020\"')[1].split('<ul>')[1].split('id=\"an2019\"')[0].split('</ul>')[0].strip('\n') 
        else: a_2020 = ""
        a_2019 = temp.split('id="an2019"')[1].split('<ul>')[1].split('id=\"an2018\"')[0].split('</ul>')[0].strip('\n') 
        a_2018 = temp.split('id="an2018"')[1].split('<ul>')[1].split('id=\"an2017\"')[0].split('</ul>')[0].strip('\n') 
        a_2017 = temp.split('id="an2017"')[1].split('<ul>')[1].split('</ul>')[0].strip('\n')  
        an_temp_dict = {"2020": a_2020, "2019": a_2019, "2018": a_2018, "2017":a_2017}
        an_url_dict = {}
        for annee in an_temp_dict.keys() :
            print("annee = ", annee)
            if an_temp_dict[annee] != '' :
                l = an_temp_dict[annee].strip('\n').strip('\n').split('<li class="row">')
                l = [a for a in l if a != ""]
                #print("l= ", l)
                an_url_dict[annee]={}
                for i, a in enumerate(l):
                    print("a avant strip = ",a)
                    a = a.strip('\n<span class="colspan titre3">').strip('\n').strip('</li>').strip('\n').strip('</span>').strip('\n')
                    print("a apres strip = ",a)
                    t = a.startswith('href')
                    print("Starts with href = ",t)
                    t = a.endswith('</a>')
                    print("Ends with </a> = ",t)
                    assert(a.startswith('href'))
                    assert(a.endswith('</a>'))
                    a = a.strip('<a').strip('</a>')
                    url = clean_legifrance_url(a.split('>')[0], prefix, const_el)
                    libelle, id_legifr = clean_libelle(a.split('>')[1])
                    d = {'url': url, 'libelle':libelle, 'id_legifr':id_legifr}
                    an_url_dict[annee][i] = d
    return an_temp_dict, an_url_dict


# In[173]:


an_temp_dict, an_url_dict = extract_DL_urls(main_url, main_filename)


# In[174]:


print(an_temp_dict["2019"])


# In[178]:


print(an_url_dict["2019"])


# In[186]:


def main():
    chemin1 = os.path.join(path, to_extract_dirname)
    #assert(os.path.isdir(chemin1))
    subprocess.run(["mkdir", chemin1])
    chemin2 = os.path.join(path, extracted_dirname)
    subprocess.run(["mkdir", chemin2])
    prefix = '''https://www.legifrance.gouv.fr/'''
    const_el = '''affichLoiPreparation.do;j'''


# In[183]:


def read_json (filename) :
    with open(filename, encoding='utf-8') as file:
        data_dict = json.load(file)
    return data_dict  

def save_json (data_dict, filename) :
    print(filename)
    if not filename :
        filename = "myjson.json"
        print(filename)
    with open(filename, mode='w', encoding='utf-8') as file:
        json.dump(data_dict, file)


# In[196]:


chemin3 = os.path.join(path, extracted_dirname, urls_dossiers)
print(chemin3)
save_json(an_url_dict, chemin3+".json")


# In[ ]:




