declare module "roman-number" {
  import RomanNumber from "roman-number"

  class roman_number {
    constructor(value: any)

    toInt(): any

    toString(): any

    static checkValidity(n: any): void
  }

  export = roman_number
}
