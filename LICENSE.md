# Tricoteuses-Dossiers-Législatifs

## _Generate Git repositories for the bills of the French Parliament_

By:
* Agnieszka Ślusarczyk <mailto:agneska@agneska.fr>
* Emmanuel Raviart <mailto:emmanuel@raviart.com>

Copyright (C) 2019, 2020, 2021 Agnieszka Ślusarczyk & Emmanuel Raviart

https://git.en-root.org/tricoteuses/tricoteuses-dossiers-legislatifs

> Tricoteuses-Dossiers-Législatifs is free software; you can redistribute
> it and/or modify it under the terms of the GNU Affero General Public
> License as published by the Free Software Foundation, either version 3
> of the License, or (at your option) any later version.
>
> Tricoteuses-Dossiers-Législatifs is distributed in the hope that it
> will be useful, but WITHOUT ANY WARRANTY; without even the implied
> warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
> See the GNU Affero General Public License for more details.
>
> You should have received a copy of the GNU Affero General Public License
> along with this program. If not, see <http://www.gnu.org/licenses/>.
