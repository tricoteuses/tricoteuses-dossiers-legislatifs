import {
  ActeLegislatif,
  CodeTypeOrgane,
  Document,
  Organe,
  shortNameFromOrgane,
  TypeActeLegislatif,
} from "@tricoteuses/assemblee"

import {
  dateFromNullableDateOrIsoString,
  dateFromYearMonthDayTimezoneString,
  frenchTimeFromDateOrString,
  localIso8601StringFromDate,
} from "../dates"
import {
  AssembleeStep,
  DateBlock,
  Dossier,
  StepActe,
  StepCodeTypeOrgane,
  StepOnlyCodeTypeOrgane,
  StepOnlyTypeActe,
  StepTypeActe,
} from "../types"

interface Context {
  texteLoi?: Document
}

/// Group steps by date, organe & sub-organe.
export function chronologyFromAssembleeSteps(steps: AssembleeStep[]): DateBlock[] {
  let currentDateLocalIsoString = null
  let dateBlock: DateBlock | null = null
  const dateBlocks: DateBlock[] = []
  for (const step of steps) {
    const dateLocalIsoString =
      step.date === undefined ? undefined : localIso8601StringFromDate(step.date)
    if (dateLocalIsoString !== currentDateLocalIsoString) {
      currentDateLocalIsoString = dateLocalIsoString
      dateBlock = {
        dateLocalIsoString,
        keys: [],
        organeBlockByKey: {},
      }
      dateBlocks.push(dateBlock)
    }
    const organeBlockKey = `${step.originOrganeName || "zz"}$${step.organeName}$${
      step.title
    }`
    let organeBlock = dateBlock!.organeBlockByKey[organeBlockKey]
    if (organeBlock === undefined) {
      dateBlock!.keys.push(organeBlockKey)
      organeBlock = dateBlock!.organeBlockByKey[organeBlockKey] = {
        id: step.leafActe.uid,
        keys: [],
        organeCodeType: step.organeCodeType,
        organeName: step.organeName,
        originOrganeCodeType: step.originOrganeCodeType,
        originOrganeName: step.originOrganeName,
        subOrganeBlockByKey: {},
        title: step.title,
      }
    }
    const leafOrganeShortName = shortNameFromOrgane(step.leafActe.organe)
    const subOrganeBlockKey =
      ["SaisieComAvis_Type", "SaisieComFond_Type"].includes(step.typeActe) ||
      leafOrganeShortName === organeBlock.organeName ||
      leafOrganeShortName === organeBlock.originOrganeName ||
      leafOrganeShortName === shortNameFromOrgane(step.rootActe.organe)
        ? ""
        : leafOrganeShortName
    let subOrganeBlock = organeBlock.subOrganeBlockByKey[subOrganeBlockKey]
    if (subOrganeBlock === undefined) {
      if (subOrganeBlockKey) {
        organeBlock.keys.push(subOrganeBlockKey)
      } else {
        // The main sub-block must always be the first.
        organeBlock.keys.unshift(subOrganeBlockKey)
      }
      subOrganeBlock = organeBlock.subOrganeBlockByKey[subOrganeBlockKey] = {
        codeActe: step.leafActe.codeActe,
        id: step.leafActe.uid,
        assembleeSteps: [],
        title: subOrganeBlockKey,
      }
    }
    subOrganeBlock.assembleeSteps!.push(step)
  }

  return dateBlocks
}

export function filterAuthoritativeAssembleeStep(_step: AssembleeStep): boolean {
  // TODO
  return true
}

export function procedureAccelereeFromActes(
  actes: ActeLegislatif[],
): ActeLegislatif | null {
  for (const acte of actes) {
    if (acte.actesLegislatifs) {
      let procedureAcceleree = procedureAccelereeFromActes(acte.actesLegislatifs)
      if (procedureAcceleree !== null) {
        return procedureAcceleree
      }
    } else if (acte.xsiType === "ProcedureAccelere_Type") {
      return acte
    }
  }
  return null
}

function stepsFromActe(
  ancestors: ActeLegislatif[],
  acteOriginal: ActeLegislatif,
  context: Context,
  steps: AssembleeStep[],
) {
  const acte: StepActe = { ...acteOriginal }
  const actesLegislatifs = acte.actesLegislatifs
  delete acte.actesLegislatifs
  ancestors.push(acte)
  if (actesLegislatifs !== undefined) {
    // Occurs only when acte.xsiType === "Etape_Type"
    for (const child of actesLegislatifs) {
      stepsFromActe([...ancestors], child, context, steps)
    }
  } else {
    // Acte is a leaf acte.
    if (acte.dateActe) {
      const time = frenchTimeFromDateOrString(acte.dateActe)
      if (time !== "0h00") {
        acte.time = time
      }
    }
    const rootActe = ancestors[0]
    let organeCodeType = rootActe.organe!.codeType
    let organeName = shortNameFromOrgane(rootActe.organe)
    let originOrganeCodeType: StepCodeTypeOrgane | null = null
    let originOrganeName: string | null = null
    let title: string | null =
      rootActe.libelleActe.libelleCourt || rootActe.libelleActe.nomCanonique
    let skipRootActe = true
    switch (acte.xsiType) {
      case "DepotAvisConseilEtat_Type":
        originOrganeCodeType = StepOnlyCodeTypeOrgane.Ce
        originOrganeName = "Conseil d'État"
        // title = null
        break
      case "DepotInitiative_Type":
      case "EtudeImpact_Type": {
        const texteAssocie = acte.texteAssocie
        if (texteAssocie !== undefined) {
          if (
            texteAssocie.classification.famille !== undefined &&
            texteAssocie.classification.famille.classe.code === "PRJLOI"
          ) {
            // A "projet de loi" always come from Gouvernement.
            originOrganeCodeType = CodeTypeOrgane.Gouvernement
            originOrganeName = "Gouvernement"
          } else {
            originOrganeCodeType = StepOnlyCodeTypeOrgane.Autre
            originOrganeName = texteAssocie.auteurs
              .filter(
                auteur =>
                  auteur.acteur !== undefined && auteur.acteur.acteur !== undefined,
              )
              .map(
                ({ acteur }) =>
                  `${acteur!.acteur!.etatCivil.ident.prenom} ${
                    acteur!.acteur!.etatCivil.ident.nom
                  }`,
              )
              .join(", ")
          }
        }
        break
      }
      case "DepotInitiativeNavette_Type":
        if (acte.provenance !== undefined) {
          originOrganeCodeType = acte.provenance.codeType
          originOrganeName = shortNameFromOrgane(acte.provenance)
        }
        break
      case "ProcedureAccelere_Type":
        organeCodeType = acte.organe!.codeType
        organeName = shortNameFromOrgane(acte.organe)
        title = null
        break
      case "RenvoiCMP_Type": {
        const initiateur = acte.initiateur
        if (initiateur !== undefined) {
          let organe: Organe | null = null
          if (initiateur.acteurs !== undefined && initiateur.acteurs.length > 0) {
            let mandat = initiateur.acteurs[0].mandat
            if (
              mandat !== undefined &&
              mandat.organes !== undefined &&
              mandat.organes.length > 0
            ) {
              organe = mandat.organes[0]
            }
          } else if (initiateur.organe !== undefined) {
            organe = initiateur.organe
          }
          if (organe !== null) {
            while (organe!.organeParent) {
              organe = organe!.organeParent
            }
            originOrganeCodeType = organe.codeType
            originOrganeName = shortNameFromOrgane(organe)
          }
        }
        break
      }
      case "SaisineConseilConstit_Type":
        if (acte.organe !== undefined) {
          originOrganeCodeType = acte.organe.codeType
          originOrganeName = shortNameFromOrgane(acte.organe)
        }
        break
      default:
        break
    }

    if (acte.codeActe.startsWith("CMP-DEBATS-")) {
      const level1Acte = ancestors[1]
      organeCodeType = level1Acte.organe!.codeType
      organeName = shortNameFromOrgane(level1Acte.organe)
      title = "Lecture du texte de la Commission mixte paritaire"
    }

    if (title !== null && title.toLowerCase() === organeName.toLowerCase()) {
      title = null
    }
    // if (rootActe.codeActe === "CMP") {
    //   organeName = "Assemblée et Sénat"
    // } else if (rootActe.codeActe === "PROM") {
    //   // Promulgation de la loi par le Chef de l'État
    //   organeCodeType = "GOUVERNEMENT"
    //   organeName = "Gouvernement"
    //   title = null
    // } else if (acte.codeActe.endsWith("-AVCE")) {
    //   // Avis du Conseil d'État
    //   organeCodeType = "CE"
    //   organeName = "Conseil d'État"
    //   title = null
    // } else if (acte.codeActe.endsWith("-ETI") || acte.codeActe.endsWith("-PROCACC")) {
    //   // "-ETI": By default, "les études d'impact" come from "Gouvernement".
    //   // "-PROCACC": Le Gouvernement déclare l'urgence / engage la procédure accélérée.
    //   organeCodeType = "GOUVERNEMENT"
    //   organeName = "Gouvernement"
    //   title = null
    // }

    // Move documents in acte context.texteLoi
    if (acte.texteAdopte) {
      console.assert(acte.texteAdopte.xsiType === "texteLoi_Type")
      context.texteLoi = acte.texteAdopte
    }
    if (acte.texteAssocie) {
      if (acte.texteAssocie.xsiType === "texteLoi_Type") {
        context.texteLoi = acte.texteAssocie
      }
    }

    const step: AssembleeStep = {
      actes: ancestors,
      date: dateFromNullableDateOrIsoString(acte.dateActe, undefined)!,
      index: -1, // Real index will be put later.
      leafActe: acte,
      organeCodeType,
      organeName,
      originOrganeCodeType:
        originOrganeCodeType === null ? undefined : originOrganeCodeType,
      originOrganeName: originOrganeName === null ? undefined : originOrganeName,
      rootActe,
      skipRootActe,
      texteLoi: context.texteLoi,
      title: title === null ? undefined : title,
      typeActe: acte.xsiType,
    }
    steps.push(step)
    if (acte.xsiType == "EtudeImpact_Type" && acte.contributionInternaute) {
      // Consultation des internautes
      if (acte.contributionInternaute.dateOuverture) {
        steps.push({
          ...step,
          date: dateFromYearMonthDayTimezoneString(
            acte.contributionInternaute.dateOuverture,
          ),
          organeCodeType: StepOnlyCodeTypeOrgane.Internet,
          organeName: "Internet",
          originOrganeCodeType: step.organeCodeType,
          originOrganeName: step.organeName,
          title: "Consultation",
          typeActe: StepOnlyTypeActe.EtudeImpactContributionInternauteOuverture,
        })
      }
      if (acte.contributionInternaute.dateFermeture) {
        steps.push({
          ...step,
          date: dateFromYearMonthDayTimezoneString(
            acte.contributionInternaute.dateFermeture,
          ),
          originOrganeCodeType: StepOnlyCodeTypeOrgane.Internet,
          originOrganeName: "Internet",
          title: "Consultation",
          typeActe: StepOnlyTypeActe.EtudeImpactContributionInternauteFermeture,
        })
      }
    } else if (acte.xsiType == "Promulgation_Type" && acte.infoJo) {
      steps.push({
        ...step,
        date: dateFromYearMonthDayTimezoneString(acte.infoJo.dateJo),
        typeActe: StepOnlyTypeActe.PromulgationInfoJo,
      })
      // Handle "publications rectificatives au Journal officiel"
      for (const infoJo of acte.infoJoRect || []) {
        steps.push({
          ...step,
          date: dateFromYearMonthDayTimezoneString(infoJo.dateJo),
          infoJo,
          typeActe: StepOnlyTypeActe.PromulgationInfoJoRect,
        })
      }
    }
  }
}

/// Convert tree of "actes" in "dossier" to sequential steps.
export function stepsFromDossier(dossier: Dossier): AssembleeStep[] {
  const steps: AssembleeStep[] = []
  for (const acte of dossier.actesLegislatifs) {
    const actesSequence: ActeLegislatif[] = []
    const context: Context = {}
    stepsFromActe(actesSequence, acte, context, steps)
  }

  // Sort steps by date.
  // Before sort, add an index to each step to be sure to have a stable sort by date.
  steps.map((step, index) => {
    step.index = index
    return step
  })
  steps.sort((step1, step2) => {
    if (step1.date === undefined || step2.date === undefined) {
      // When a step has no date, keep the original order.
      return step1.index - step2.index
    }
    // Ensure that dates without time are sorted after dates with time.
    const date1 = step1.date.toISOString().replace(/T00:00:00/, "T99:99:99")
    const date2 = step2.date.toISOString().replace(/T00:00:00/, "T99:99:99")
    return date1 < date2 ? -1 : date1 === date2 ? step1.index - step2.index : 1
  })

  return steps
}

/// Convert tree of "actes" in "dossier" to sequential steps and keep only the steps
/// corresponding to "textes de lois" and for which Assemblée is relevant.
export function assembleeTextesStepsFromDossier(dossier: Dossier): AssembleeStep[] {
  const steps = stepsFromDossier(dossier)
    .filter(filterAuthoritativeAssembleeStep)
    .filter((step: AssembleeStep) => {
      if (
        !([
          TypeActeLegislatif.DepotInitiativeNavetteType,
          TypeActeLegislatif.DepotInitiativeType,
          TypeActeLegislatif.DepotLettreRectificativeType,
          TypeActeLegislatif.DepotRapportType,
        ] as StepTypeActe[]).includes(step.typeActe)
      ) {
        return false
      }
      if (
        step.typeActe === TypeActeLegislatif.DepotRapportType &&
        step.leafActe.texteAdopte === undefined
      ) {
        // Ignore "rapports" without "texte adopté".
        return false
      }
      return true
    })
    .map((step: AssembleeStep) => {
      if (
        step.typeActe === TypeActeLegislatif.DepotRapportType &&
        step.leafActe.texteAssocie !== undefined
      ) {
        step = { ...step }
        // Keep only "texte adopté" in "rapports".
        delete step.leafActe.texteAssocie
      }
      return step
    })
  return steps
}
