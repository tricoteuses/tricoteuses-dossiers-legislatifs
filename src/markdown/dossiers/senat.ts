import { Ecr } from "@tricoteuses/senat"
import RomanNumber from "roman-number"

import config from "../../config"
import {
  frenchDateWithoutDayOfWeekFromDateOrString,
  frenchTimeFromDateOrString,
} from "../../dates"
import { SenatStep, SenatStepKind } from "../../types"

export function colorFromEtaloilib(etaloilib: string): string {
  return etaloilib === "en cours de discussion"
    ? "#718096" // gray-600
    : etaloilib === "fusionné"
    ? "#4299e1" // blue-500
    : etaloilib === "rejeté"
    ? "#f56565" // red-500
    : etaloilib === "promulgué ou adopté (ppr)"
    ? "#48bb78" // green-500
    : etaloilib === "caduc"
    ? "#4299e1" // blue-500
    : "#ecc94b" // yellow-500
}

export function emojiFromEtaloilib(etaloilib: string): string {
  return etaloilib === "en cours de discussion"
    ? ":arrow_right:"
    : etaloilib === "fusionné"
    ? ":recycle:"
    : etaloilib === "rejeté"
    ? ":x:"
    : etaloilib === "promulgué ou adopté (ppr)"
    ? ":white_check_mark:"
    : etaloilib === "caduc"
    ? ":recycle:"
    : ":arrow_right:"
}

function markDownLineFromEcrs(ecrs: Ecr[]): string {
  const fragments = []
  const cosignataires = ecrs.filter(ecr => ecr.signataire === "n")
  const presidentOrPrimoSignataire = ecrs.filter(ecr => ecr.signataire !== "n")
  if (presidentOrPrimoSignataire.length > 0) {
    for (const [ecrIndex, ecr] of presidentOrPrimoSignataire.entries()) {
      const aut = ecr.aut!
      fragments.push(`${aut.qua!.quaabr} ${aut.prenom} ${aut.nomuse}`)
      if (aut.autfct) {
        fragments.push(`, ${aut.autfct}`)
      }
      if (ecrIndex > 0 && ecrIndex < presidentOrPrimoSignataire.length - 2) {
        fragments.push(", ")
      } else if (ecrIndex == presidentOrPrimoSignataire.length - 2) {
        if (cosignataires.length > 0) {
          fragments.push(", ")
        } else {
          fragments.push(" et ")
        }
      }
    }
    if (cosignataires.length > 0) {
      if (cosignataires.length > 1) {
        if (presidentOrPrimoSignataire.length > 1) {
          fragments.push(" et plusieurs de leurs collègues")
        } else {
          fragments.push(" et plusieurs de ses collègues")
        }
      } else if (presidentOrPrimoSignataire.length > 1) {
        fragments.push(" et un de leurs collègues")
      } else {
        fragments.push(" et un de ses collègues")
      }
    }
  } else {
    for (const [ecrIndex, ecr] of ecrs.entries()) {
      const aut = ecr.aut!
      fragments.push(`${aut.qua!.quaabr} ${aut.prenom} ${aut.nomuse}`)
      if (aut.autfct) {
        fragments.push(`, ${aut.autfct}`)
      }
      if (ecrIndex > 0 && ecrIndex < ecrs.length - 2) {
        fragments.push(", ")
      } else if (ecrIndex == ecrs.length - 2) {
        fragments.push(" et ")
      }
    }
  }
  return fragments.join("")
}

export function markdownLinesFromSenatStep(lines: string[], step: SenatStep): void {
  switch (step.kind) {
    case SenatStepKind.AmendementsCommission: {
      const fragments = [
        "Dépôt des",
        `[amendements](${new URL(
          `discussions/senat/${step.lecassamesescom}/${step.lecassamecom}`,
          config.uiUrl,
        ).toString()})`,
        "en vue de l'élaboration du texte de la commission",
      ]
      if (step.lecassamecomdat !== null) {
        fragments.push(
          `(délai limite : ${frenchTimeFromDateOrString(step.lecassamecomdat)})`,
        )
      }
      lines.push(fragments.join(" "))
      lines.push("")
      break
    }
    case SenatStepKind.AmendementsSeance: {
      const fragments = [
        "Dépôt des",
        `[amendements](${new URL(
          `discussions/senat/${step.lecassameses}/${step.lecassame}`,
          config.uiUrl,
        ).toString()})`,
        "sur le texte de la commission",
      ]
      if (step.lecassamedat !== null) {
        fragments.push(
          `(délai limite : ${frenchTimeFromDateOrString(step.lecassamedat)})`,
        )
      }
      lines.push(fragments.join(" "))
      lines.push("")
      break
    }
    case SenatStepKind.Aud: {
      lines.push(
        `[${step.audtit}](${new URL(step.audurl, "https://www.senat.fr/").toString()})`,
      )
      lines.push("")
      break
    }
    case SenatStepKind.DateSeance: {
      const lineFragments = ["Séance publique"]
      if (step.statut) {
        lineFragments.push(
          `(${step.statut === "EVENT" ? "éventuellement" : step.statut})`,
        )
      }
      lines.push(lineFragments.join(" "))
      lines.push("")
      const debat = step.debat
      if (debat !== undefined && debat.deburl !== null) {
        lines.push(
          `* [Compte rendu intégral des débats](${new URL(
            debat.deburl,
            "https://www.senat.fr/seances/",
          ).toString()})`,
        )
        lines.push("")
      }
      if (step.scrs !== undefined) {
        for (const scr of step.scrs) {
          const lineFragments = [
            `[Scrutin public n° ${scr.scrnum}](https://www.senat.fr/scrutin-public/${scr.sesann}/scr${scr.sesann}-${scr.scrnum}.html)`,
            scr.scrint,
          ]
          if (scr.scrpou !== null && scr.scrcon !== null) {
            if (scr.scrpou > scr.scrcon) {
              lineFragments.push("adopté :white_check_mark:")
            } else if (scr.scrpou === scr.scrcon) {
              lineFragments.push("indécis :interrobang:")
            } else {
              lineFragments.push("rejeté :x:")
            }
          }
          lines.push("* " + lineFragments.join(" "))
          lines.push("")
        }
      }
      break
    }
    case SenatStepKind.DecisionConseilConstitutionnel: {
      let decision = `Décision n° ${step.num_decision}`
      if (step.deccocurl) {
        decision = `[${decision}](${step.deccocurl})`
      }
      const lineFragments = [decision, ":"]
      if (step.deccoc !== undefined) {
        lineFragments.push(step.deccoc.deccoclib)
        lineFragments.push(
          step.deccoc.deccoccod == "01" // conforme
            ? ":white_check_mark:"
            : step.deccoc.deccoccod == "02" // partiellement conforme
            ? ":white_check_mark: :x:"
            : step.deccoc.deccoccod == "03" // non conforme
            ? ":x:"
            : // step.deccoc.deccoccod == "04" // se déclare incompétent
              ":interrobang:",
        )
      }
      lines.push(lineFragments.join(" "))
      break
    }
    case SenatStepKind.EngagementProcedureAcceleree: {
      lines.push("Le gouvernement engage la procédure accélérée")
      lines.push("")
      break
    }
    case SenatStepKind.LecAssRap: {
      const rap = step.rap!
      const titleFragments = []
      const denrap = rap.denrap
      if (denrap !== undefined) {
        titleFragments.push(denrap.libdenrap)
      }
      titleFragments.push("n°", rap.rapnum)
      const ass = step.lecass.ass
      if (ass !== undefined && ass.codass === "S") {
        titleFragments.push(`(${rap.sesann}-${rap.sesann + 1})`)
      }
      if (rap.numerobis) {
        titleFragments.push(rap.numerobis)
      }
      if (rap.raptom) {
        titleFragments.push(`tome ${new RomanNumber(rap.raptom).toString()}`)
      }
      let title = titleFragments.join(" ")
      if (rap.rapurl !== null) {
        title = `[${title}](${new URL(
          rap.rapurl,
          "https://www.senat.fr/rap/",
        ).toString()})`
      }
      const lineFragments = [title]
      const ecrs = rap.ecrs
      if (ecrs !== undefined && ecrs.length > 0) {
        lineFragments.push("de")
        lineFragments.push(markDownLineFromEcrs(ecrs))
      }
      lines.push(lineFragments.join(" "))
      lines.push("")
      if (rap.docatts !== undefined) {
        for (const docatt of rap.docatts) {
          let docattTitle =
            docatt.typattcod === "S" ? "Synthèse du rapport" : docatt.typatt!.typattlib
          if (docatt.docatturl !== null) {
            docattTitle = `[${docattTitle}](${new URL(
              docatt.docatturl,
              "https://www.senat.fr/",
            ).toString()})`
          }
          lines.push("* " + docattTitle)
          lines.push("")
        }
      }
      break
    }
    case SenatStepKind.OppositionProcedureAcceleree: {
      lines.push("Opposition à la procédure accélérée")
      lines.push("")
      break
    }
    case SenatStepKind.PromulgationLoi: {
      lines.push(`Promulgation de la loi n° ${step.numero}`)
      lines.push("")
      break
    }
    case SenatStepKind.PublicationLoi: {
      const fragments = [
        "Parution au",
        `[Journal officiel n° ${step.loinumjo}](${step.url_jo})`,
        `de la loi n° ${step.numero}`,
      ]
      if (step.date_loi !== null) {
        fragments.push(`du ${frenchDateWithoutDayOfWeekFromDateOrString(step.date_loi)}`)
      }
      if (step.loititjo !== null) {
        fragments.push(step.loititjo)
      }
      lines.push(fragments.join(" "))
      lines.push("")
      break
    }
    case SenatStepKind.PublicationPremierCorrectifLoi: {
      const fragments = [
        "Parution d'un correctif au",
        `[Journal officiel n° ${step.loinumjo2}](${step.url_jo2})`,
      ]
      lines.push(fragments.join(" "))
      lines.push("")
      break
    }
    case SenatStepKind.PublicationSecondCorrectifLoi: {
      const fragments = [
        "Parution d'un seconde correctif au",
        `[Journal officiel n° ${step.loinumjo3}](${step.url_jo3})`,
      ]
      lines.push(fragments.join(" "))
      lines.push("")
      break
    }
    case SenatStepKind.RetraitProcedureAcceleree: {
      lines.push("Retrait de la procédure accélérée")
      lines.push("")
      break
    }
    case SenatStepKind.SaisineConseilConstitutionnel: {
      lines.push(`Saisine par ${step.saisine_par}`)
      lines.push("")
      break
    }
    case SenatStepKind.Texte: {
      const fragments = ["Texte n°", step.texnum]
      if (step.sesann !== null) {
        fragments.push(`(${step.sesann}-${step.sesann + 1})`)
      }
      if (step.numerobis) {
        fragments.push(step.numerobis)
      }
      const title = `[${fragments.join(" ")}](${new URL(
        `documents/senat/${step.texcod}`,
        config.uiUrl,
      ).toString()})`
      const lineFragments = [title]
      const ecrs = step.ecrs
      if (ecrs !== undefined && ecrs.length > 0) {
        lineFragments.push("de")
        lineFragments.push(markDownLineFromEcrs(ecrs))
      }
      lineFragments.push(
        step.oritxt
          ? step.oritxt.oritxtlib.endsWith(" (AN)")
            ? step.oritxt.oritxtlib.substring(
                0,
                step.oritxt.oritxtlib.length - " (AN)".length,
              )
            : step.oritxt.oritxtlib
          : "déposé",
      )
      if (step.oritxt) {
        if (step.oritxt.oritxtado === "O") {
          if (step.oritxt.oritxtmod === "O") {
            lineFragments.push("adopté après modification :x: :white_check_mark:")
          } else {
            lineFragments.push("adopté :white_check_mark:")
          }
        } else if (step.oritxt.oritxtado === "N" && step.oritxt.oriordre !== "0") {
          // Show "rejeté" only when oriordre != 0 (ie "initial", "déposé").
          lineFragments.push("rejeté :x:")
        }
      }
      lines.push(lineFragments.join(" "))
      lines.push("")
      break
    }
  }
}
