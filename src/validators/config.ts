import { validateNonEmptyTrimmedString, validateUrl } from "@biryani/core"

export function validateConfig(data: any): [any, any] {
  if (data === null || data === undefined) {
    return [data, "Missing value"]
  }
  if (typeof data !== "object") {
    return [data, `Expected an object got "${typeof data}"`]
  }

  data = { ...data }
  const errors: { [key: string]: any } = {}
  const remainingKeys = new Set(Object.keys(data))

  for (const key of ["assembleeApiUrl", "senatApiUrl", "uiUrl"]) {
    remainingKeys.delete(key)
    const [value, error] = validateUrl(data[key])
    data[key] = value
    if (error !== null) {
      errors[key] = error
    }
  }

  {
    const key = "gitlab"
    remainingKeys.delete(key)
    const [value, error] = validateGitlab(data[key])
    data[key] = value
    if (error !== null) {
      errors[key] = error
    }
  }

  for (const key of remainingKeys) {
    errors[key] = "Unexpected item"
  }
  return [data, Object.keys(errors).length === 0 ? null : errors]
}

function validateGitlab(data: any): [any, any] {
  if (data === null || data === undefined) {
    return [data, "Missing value"]
  }
  if (typeof data !== "object") {
    return [data, `Expected an object got "${typeof data}"`]
  }

  data = { ...data }
  const errors: { [key: string]: any } = {}
  const remainingKeys = new Set(Object.keys(data))

  for (let key of ["accessToken", "groupPath", "userEmail", "username"]) {
    remainingKeys.delete(key)
    const [value, error] = validateNonEmptyTrimmedString(data[key])
    data[key] = value
    if (error !== null) {
      errors[key] = error
    }
  }

  {
    const key = "url"
    remainingKeys.delete(key)
    const [value, error] = validateUrl(data[key])
    data[key] = value
    if (error !== null) {
      errors[key] = error
    }
  }

  for (let key of remainingKeys) {
    errors[key] = "Unexpected item"
  }
  return [data, Object.keys(errors).length === 0 ? null : errors]
}
