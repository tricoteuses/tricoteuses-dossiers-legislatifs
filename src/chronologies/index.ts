export {
  assembleeTextesStepsFromDossier,
  chronologyFromAssembleeSteps,
  filterAuthoritativeAssembleeStep,
  procedureAccelereeFromActes,
  stepsFromDossier,
} from "./assemblee"
export {
  chronologyFromSenatSteps,
  filterAuthoritativeSenatStep,
  senatTextesStepsFromLoi,
  stepsFromLoi,
} from "./senat"
import { DateBlock, OrganeBlock, SubOrganeBlock } from "../types"

export function mergeAssembleeAndSenatChronologies(
  assembleeChronology: DateBlock[] | null,
  senatChronology: DateBlock[] | null,
): DateBlock[] {
  let chronology
  if (assembleeChronology === null) {
    chronology = senatChronology
  } else if (senatChronology === null) {
    chronology = assembleeChronology
  } else {
    chronology = []
    let assembleeDateBlockIndex = 0
    let senatDateBlockIndex = 0
    for (;;) {
      const assembleeDateBlock = assembleeChronology[assembleeDateBlockIndex]
      const senatDateBlock = senatChronology[senatDateBlockIndex]
      if (assembleeDateBlock === undefined && senatDateBlock === undefined) {
        break
      } else if (assembleeDateBlock === undefined) {
        chronology.push(senatDateBlock)
        senatDateBlockIndex++
      } else if (senatDateBlock === undefined) {
        chronology.push(assembleeDateBlock)
        assembleeDateBlockIndex++
      } else if (
        assembleeDateBlock.dateLocalIsoString === senatDateBlock.dateLocalIsoString
      ) {
        const keys: string[] = []
        const organeBlockByKey: { [key: string]: OrganeBlock } = {}
        const dateBlock: DateBlock = {
          dateLocalIsoString: assembleeDateBlock.dateLocalIsoString,
          keys,
          organeBlockByKey,
        }
        chronology.push(dateBlock)
        assembleeDateBlockIndex++
        senatDateBlockIndex++

        let assembleeOrganeBlockIndex = 0
        let senatOrganeBlockIndex = 0
        for (;;) {
          const assembleeOrganeBlockKey =
            assembleeDateBlock.keys[assembleeOrganeBlockIndex]
          const senatOrganeBlockKey = senatDateBlock.keys[senatOrganeBlockIndex]
          if (
            assembleeOrganeBlockKey === undefined &&
            senatOrganeBlockKey === undefined
          ) {
            break
          } else if (assembleeOrganeBlockKey === undefined) {
            dateBlock.keys.push(senatOrganeBlockKey)
            dateBlock.organeBlockByKey[senatOrganeBlockKey] =
              senatDateBlock.organeBlockByKey[senatOrganeBlockKey]
            senatOrganeBlockIndex++
          } else if (senatOrganeBlockKey === undefined) {
            dateBlock.keys.push(assembleeOrganeBlockKey)
            dateBlock.organeBlockByKey[assembleeOrganeBlockKey] =
              assembleeDateBlock.organeBlockByKey[assembleeOrganeBlockKey]
            assembleeOrganeBlockIndex++
          } else if (assembleeOrganeBlockKey === senatOrganeBlockKey) {
            const assembleeOrganeBlock =
              assembleeDateBlock.organeBlockByKey[assembleeOrganeBlockKey]
            const senatOrganeBlock = senatDateBlock.organeBlockByKey[senatOrganeBlockKey]
            console.assert(
              assembleeOrganeBlock.organeCodeType === senatOrganeBlock.organeCodeType,
            )
            console.assert(
              assembleeOrganeBlock.organeName === senatOrganeBlock.organeName,
            )
            console.assert(
              assembleeOrganeBlock.originOrganeCodeType ===
                senatOrganeBlock.originOrganeCodeType,
            )
            console.assert(
              assembleeOrganeBlock.originOrganeName === senatOrganeBlock.originOrganeName,
            )
            console.assert(assembleeOrganeBlock.title === senatOrganeBlock.title)
            const subKeys: string[] = []
            const subOrganeBlockByKey: { [key: string]: SubOrganeBlock } = {}
            const organeBlock: OrganeBlock = {
              id: assembleeOrganeBlock.id, // Used only by Assemblée. TODO: Remove or keep?
              keys: subKeys,
              organeCodeType: assembleeOrganeBlock.organeCodeType,
              organeName: assembleeOrganeBlock.organeName,
              originOrganeCodeType: assembleeOrganeBlock.originOrganeCodeType,
              originOrganeName: assembleeOrganeBlock.originOrganeName,
              subOrganeBlockByKey,
              title: assembleeOrganeBlock.title,
            }
            dateBlock.keys.push(assembleeOrganeBlockKey)
            dateBlock.organeBlockByKey[assembleeOrganeBlockKey] = organeBlock
            assembleeOrganeBlockIndex++
            senatOrganeBlockIndex++

            let assembleeSubOrganeBlockIndex = 0
            let senatSubOrganeBlockIndex = 0
            for (;;) {
              const assembleeSubOrganeBlockKey =
                assembleeOrganeBlock.keys[assembleeSubOrganeBlockIndex]
              const senatSubOrganeBlockKey =
                senatOrganeBlock.keys[senatSubOrganeBlockIndex]
              if (
                assembleeSubOrganeBlockKey === undefined &&
                senatSubOrganeBlockKey === undefined
              ) {
                break
              } else if (assembleeSubOrganeBlockKey === undefined) {
                organeBlock.keys.push(senatSubOrganeBlockKey)
                organeBlock.subOrganeBlockByKey[senatSubOrganeBlockKey] =
                  senatOrganeBlock.subOrganeBlockByKey[senatSubOrganeBlockKey]
                senatSubOrganeBlockIndex++
              } else if (senatSubOrganeBlockKey === undefined) {
                organeBlock.keys.push(assembleeSubOrganeBlockKey)
                organeBlock.subOrganeBlockByKey[assembleeSubOrganeBlockKey] =
                  assembleeOrganeBlock.subOrganeBlockByKey[assembleeSubOrganeBlockKey]
                assembleeSubOrganeBlockIndex++
              } else if (assembleeSubOrganeBlockKey === senatSubOrganeBlockKey) {
                const assembleeSubOrganeBlock =
                  assembleeOrganeBlock.subOrganeBlockByKey[assembleeSubOrganeBlockKey]
                const senatSubOrganeBlock =
                  senatOrganeBlock.subOrganeBlockByKey[senatSubOrganeBlockKey]
                console.assert(
                  assembleeSubOrganeBlock.title === senatSubOrganeBlock.title,
                )
                const subOrganeBlock: SubOrganeBlock = {
                  assembleeSteps: assembleeSubOrganeBlock.assembleeSteps,
                  codeActe: assembleeSubOrganeBlock.codeActe, // Used only by Assemblée. TODO: Remove or keep?
                  id: assembleeSubOrganeBlock.id, // Used only by Assemblée. TODO: Remove or keep?
                  senatSteps: senatSubOrganeBlock.senatSteps,
                  title: assembleeSubOrganeBlock.title,
                }
                organeBlock.keys.push(assembleeSubOrganeBlockKey)
                organeBlock.subOrganeBlockByKey[
                  assembleeSubOrganeBlockKey
                ] = subOrganeBlock
                assembleeSubOrganeBlockIndex++
                senatSubOrganeBlockIndex++
              } else {
                // assembleeSubOrganeBlockKey !== senatSubOrganeBlockKey
                let assembleeFirst = true
                if (
                  senatOrganeBlock.keys.indexOf(assembleeSubOrganeBlockKey) >
                  senatSubOrganeBlockIndex
                ) {
                  // Sénat first
                  assembleeFirst = false
                }
                if (assembleeFirst) {
                  organeBlock.keys.push(assembleeSubOrganeBlockKey)
                  organeBlock.subOrganeBlockByKey[assembleeSubOrganeBlockKey] =
                    assembleeOrganeBlock.subOrganeBlockByKey[assembleeSubOrganeBlockKey]
                  assembleeSubOrganeBlockIndex++
                } else {
                  organeBlock.keys.push(senatSubOrganeBlockKey)
                  organeBlock.subOrganeBlockByKey[senatSubOrganeBlockKey] =
                    senatOrganeBlock.subOrganeBlockByKey[senatSubOrganeBlockKey]
                  senatSubOrganeBlockIndex++
                }
              }
            }
          } else {
            // assembleeOrganeBlockKey !== senatOrganeBlockKey
            let assembleeFirst = true
            if (
              senatDateBlock.keys.indexOf(assembleeOrganeBlockKey) > senatOrganeBlockIndex
            ) {
              // Sénat first
              assembleeFirst = false
            }
            if (assembleeFirst) {
              dateBlock.keys.push(assembleeOrganeBlockKey)
              dateBlock.organeBlockByKey[assembleeOrganeBlockKey] =
                assembleeDateBlock.organeBlockByKey[assembleeOrganeBlockKey]
              assembleeOrganeBlockIndex++
            } else {
              dateBlock.keys.push(senatOrganeBlockKey)
              dateBlock.organeBlockByKey[senatOrganeBlockKey] =
                senatDateBlock.organeBlockByKey[senatOrganeBlockKey]
              senatOrganeBlockIndex++
            }
          }
        }
      } else if (assembleeDateBlock.dateLocalIsoString === undefined) {
        chronology.push(assembleeDateBlock)
        assembleeDateBlockIndex++
      } else if (senatDateBlock.dateLocalIsoString === undefined) {
        chronology.push(senatDateBlock)
        senatDateBlockIndex++
      } else if (
        assembleeDateBlock.dateLocalIsoString < senatDateBlock.dateLocalIsoString
      ) {
        chronology.push(assembleeDateBlock)
        assembleeDateBlockIndex++
      } else {
        chronology.push(senatDateBlock)
        senatDateBlockIndex++
      }
    }
  }
  return chronology!
}
